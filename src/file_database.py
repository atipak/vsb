import json
import os
import shutil
import time
import rdkit
from rdkit import Chem
import sys

separator = '\\'
config = None
assay = None
check_assay = True


# updating methods
def update_main_file_of_datasets(number=None):
    """
    Update main file about data sets. This action runs every time when content of file was changed.

    :return: Nothing
    """
    assay_folder = config['assays_folder']
    info_file = config['assays_info_file']
    if number is None:
        if os.path.exists(info_file):
            with open(info_file, "r") as file:
                f = json.load(file)
                if "actual_number" in f:
                    number = f['actual_number']
                else:
                    number = 0
        else:
            number = 0
    files = os.listdir(assay_folder)
    names = []
    for file in files:
        pt = assay_folder + os.sep + file
        if os.path.isdir(pt):
            names.append(file)
    with open(info_file, 'w') as f:
        json.dump(
            {
                'date': str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + "," + str(
                    time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(
                    time.localtime().tm_year),
                'datasets': names,
                "actual_number": number
            }, f, indent=2
        )


def update_dataset_info(name, active=None, inactive=None, metadata=None):
    """
    Update information about dataset.
    Aktualizuje informace o datasetu. Dojde k premazani celeho souboru, proto uzivatel zadava vsechny parametry funkce.
    :param name: ID of data set
    :param active: Names of active molecules in data set. If None will be used molecules from current file.
    :param inactive: Names of active molecules in data set. If None will be used molecules from current file.
    :param metadata: Metadata to data set. If None will be used molecules from current file.
    :return: String if action was successful
    """
    check_main_database_folder()
    assay_folder = config['assays_folder'] + os.sep + name
    if not os.path.exists(assay_folder):
        return 'Dataset does not exist'
    elif name == '':
        return 'Bad name'
    else:
        if active is None or inactive is None or metadata is None:
            with open(assay_folder, 'r') as assay:
                j_file = json.load(assay)
                if metadata is None:
                    metadata = j_file['metadata']
                if active is None:
                    active = j_file['data']['molecules']['active']
                if inactive is None:
                    inactive = j_file['data']['molecules']['inactive']
        path_info = assay_folder + os.sep + 'info.json'
        dirname = os.path.dirname(path_info)
        path_info_old = dirname + separator + "info_(" + str(time.localtime().tm_hour) + "_" + str(
            time.localtime().tm_min) + "#" + str(time.localtime().tm_mday) + "_" + str(
            time.localtime().tm_mon) + "_" + str(time.localtime().tm_year) + ")" + '.json'
        os.replace(path_info, path_info_old)
        with open(path_info, 'w') as output:
            json.dump(
                {
                    'metadata': metadata,
                    'data': {
                        'molecules': {
                            'active': active,
                            'inactive': inactive
                        }
                    }
                }, output, indent=2
            )
        text = name + ' was updated'
        log('update_dataset_info', text)
        return text


def update_dataset_sdf(name, sdf):
    """
    Update SDF file with molecule in given data set.
    :param name: Name of data set
    :param sdf: String which contains data which should be written in the file.
    :return: Success of writing.
    """
    check_main_database_folder()
    path_tmp = config['assays_folder'] + separator + str(name)
    if not os.path.exists(path_tmp):
        return 'Data set does not exist'
    elif name == '' or name is None or sdf is None or not isinstance(sdf, str):
        return 'Bad name'
    else:
        path_sdf = path_tmp + os.sep + 'mols.sdf'
        path_sdf_old = path_sdf + "(" + str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + "," + str(
            time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(time.localtime().tm_year) + ")"
        os.rename(path_sdf, path_sdf_old)
        path_sdf = path_tmp + os.sep + 'mols.sdf'
        with open(path_sdf, 'w') as output:
            output.write(sdf)
        text = name + ' was updated'
        log('update_dataset_sdf', text)
        return text


def update_selections_info(dataset_id, new_id=None):
    """
    Update a file with information about selection under given data set.

    :param dataset_id: Name of data set
    :return: Nothing
    """
    path_assay = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    path_info = path_assay + os.sep + "info.json"
    files = os.listdir(path_assay)
    names = []
    for file in files:
        if not file == "info.json":
            names.append(file)
    if new_id is None:
        if os.path.exists(path_info):
            with open(path_info, "r") as file:
                f = json.load(file)
                if "actual_number" in f:
                    new_id = f['actual_number']
                else:
                    new_id = 0
        else:
            new_id = 0
    with open(path_info, 'w') as f:
        json.dump(
            {
                'date': str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + "," + str(
                    time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(
                    time.localtime().tm_year),
                'selection': names,
                "actual_number": new_id
            }, f, indent=2
        )


# addding methods
def add_result_file(original_path, test_path, id_result):
    """
    Copy file to database.

    :param original_path: Original file.
    :param test_path: Test folder.
    :param id_result: ID of subtest
    :return: new path to the file
    """
    result_file_path = test_path + os.sep + 'test_' + str(id_result) + '.json'
    shutil.copyfile(original_path, result_file_path)
    return result_file_path


def add_analysis_file(original_path, test_path, id_result):
    """
    Copy file to database.

    :param original_path: Original file.
    :param test_path: Test folder.
    :param id_result: ID of subtest
    :return: new path to the file
    :return:
    """
    analysis_file_path = os.path.join(test_path, str("analyze_{0}.json").format(id_result))
    shutil.copyfile(original_path, analysis_file_path)
    return analysis_file_path


def change_result_number(number=1):
    """
    Increase the number of finished subtests in information file of database.
    :param number: Count of new tests
    :return: Current number of results
    """
    check_results_folder()
    path_res = config['results_folder']
    path_info = config['results_info_file']
    names = []
    # reading original information
    with open(path_info, 'r') as file:
        reader = json.load(file)
        for name in reader['tests']:
            names.append(name)
        number_test = int(reader['number of tests'])
        number_result = int(reader['number of results']) + number
    # writing updated information
    with open(path_info, 'w') as f:
        json.dump(
            {
                'date': str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + "," + str(
                    time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(
                    time.localtime().tm_year),
                'tests': names,
                'number of tests': number_test,
                'number of results': number_result
            }, f, indent=2
        )
    string = "Number of result was changed on " + str(number_result)
    log("change_result_number", string)
    return number_result


def change_test_number(number=1):
    """
      Increase the number of tests in information file of database.
      :param number: Count of new tests
      :return: Current number of tests
      """
    path_info = config['results_info_file']
    names = []
    with open(path_info, 'r') as file:
        reader = json.load(file)
        for name in reader['tests']:
            names.append(name)
        number_test = reader['number of tests']
        number_result = int(reader['number of results'])
        number_test = int(number_test) + number
    with open(path_info, 'w') as f:
        json.dump(
            {
                'date': str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + "," + str(
                    time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(
                    time.localtime().tm_year),
                'tests': names,
                'number of tests': number_test,
                'number of results': number_result
            }, f, indent=2
        )
    return number_test


def add_data_set(active, inactive, metadata, sdf, name=None, sdf_string=False):
    """
    Adds a new data set into file DB.

    :param active: Names of active molecules
    :param inactive: Names of inactive molecules
    :param metadata: Metadata of data set
    :param sdf: Path of file with sdf description or string with sdf description
    :return: Name of new data set (success) or None
    """
    check_main_database_folder()
    safe_id = get_safe_id_for_data_set()
    update_main_file_of_datasets(safe_id)
    if name is None:
        if "datasetID" in metadata:
            name = metadata['datasetID']
        elif "datasetName" in metadata:
            name = metadata['datasetName']
    if name is None or name == "":
        import datetime
        name = datetime.date()
        name = name.year + "-" + name.month + "-" + name.day
    if str(name).endswith(".json"):
        name = str(name)
        index = name.rfind(".")
        name = name[:index]
    name = name + "_" + str(safe_id)
    path_tmp = config['assays_folder'] + os.sep + str(name)
    if os.path.exists(path_tmp):
        return 'Dataset exists', False
    else:
        os.mkdir(path_tmp)
        path_sel = path_tmp + os.sep + "selection"
        path_sel_info = path_sel + os.sep + "info.json"
        os.makedirs(path_sel)
        with open(path_sel_info, 'w') as o:
            json.dump({}, o, indent=2)
        # metadata updating
        if not "numberActives" in metadata:
            metadata["numberActives"] = len(active)
        if not "numberInactives" in metadata:
            metadata["numberInactives"] = len(inactive)
        path_info = path_tmp + os.sep + 'info.json'
        with open(path_info, 'w') as output:
            json.dump(
                {
                    'metadata': metadata,
                    'data': {
                        'molecules': {
                            'active': active,
                            'inactive': inactive
                        }
                    }
                }, output, indent=2
            )
        path_sdf = path_tmp + os.sep + 'mols.sdf'
        if sdf_string:
            if isinstance(sdf, str):
                with open(path_sdf, 'w') as output:
                    output.write(sdf)
            else:
                print("Sdf parameter isn't a string")
                return None, False
        else:
            try:
                if os.path.exists(sdf):
                    import shutil
                    shutil.copy(sdf, path_sdf)
                else:
                    print("Sdf path doesn't exist")
                    return None, False
            except ValueError as ve:
                print(ve)
                with open(path_sdf, 'w') as output:
                    output.write(sdf)
        text = name + ' was created'
        log('add_data_set', text)
        global check_assay
        check_assay = True
        return name, True


def add_selection(dataset_id, metadata, train_active, train_inactive, test, validation_active=[],
                  validation_inactive=[]):
    """
    Adds a new selection into file DB in data set by given ID.

    :param validation_inactive: Inactive molecules belonging to validation set
    :param validation_active: Active molecules belonging to validation set
    :param dataset_id: ID of data set where the selection should be inserted
    :param metadata: Metadata of selection
    :param train_active: Active molecules belonging to train set
    :param train_inactive: Inactive molecules belonging to train set
    :param test: Test molecules
    :return: Name of selection (success) or None
    """
    sels_path = os.path.join(config['assays_folder'], str(dataset_id), "selection")
    if os.path.exists(sels_path):
        name = get_safe_id_for_selection(dataset_id)
        sel_path = os.path.join(sels_path, str(name))
        if not os.path.exists(sel_path):
            os.mkdir(sel_path)
            info_path = os.path.join(sel_path, "info.json")
            # changing metadata
            if "train_active_number" not in metadata:
                metadata['train_active_number'] = len(train_active)
            if "train_inactive_number" not in metadata:
                metadata['train_inactive_number'] = len(train_inactive)
            if "validation_active_number" not in metadata:
                metadata['validation_active_number'] = len(validation_active)
            if "validation_inactive_number" not in metadata:
                metadata['validation_inactive_number'] = len(validation_inactive)
            if "test_number" not in metadata:
                metadata['test_number'] = len(test)
            with open(info_path, 'w') as output:
                json.dump(
                    {
                        'metadata': metadata,
                        'data': {
                            'molecules': {
                                'train': {
                                    'actives': train_active,
                                    'inactives': train_inactive
                                },
                                'test': test,
                                'validation': {
                                    'actives': validation_active,
                                    'inactives': validation_inactive
                                }
                            }
                        }
                    }, output, indent=2
                )
            update_selections_info(dataset_id, name)
            text = "Selection with ID " + str(name) + " was created"
            log('add_selection', text)
            return name, True
        else:
            print("Selection with ID " + str(name) + " already exists")
            return None, False
    else:
        print("Data set does not exist")
        return None, False


def add_method_file_new_version(original_path, method_id):
    """
    Add method file to the folder for methods under new method_id_name.

    :param original_path: Original path of method
    :param method_id: Name (id) for the given method
    :return: True/False, New path for the method file/Empty string
    """
    if os.path.exists(original_path) and os.path.isdir(original_path):
        methods_folder = get_methods_folder()
        new_file_path = methods_folder + os.sep + method_id
        shutil.copytree(original_path, new_file_path)
        return True, new_file_path
    else:
        return False, print("A bad path")


def new_test(id_test):
    """
    Creates a new folder for files which belong to the test.

    :param id_test: Name of new folder.
    :return: New path to the folder
    """
    check_results_folder()
    path_res = config['results_folder']
    path_test = path_res + os.sep + str(id_test)
    os.mkdir(path_test)
    change_test_number(1)
    string = "New test directory with number " + str(id_test) + " was created"
    log("new_test", string)
    return path_test


# deleting methods
def delete_dataset(name):
    """
    Deletes data set from database.
    :param name: Name of data set
    :return: String with result of deleting
    """
    path_tmp = config['assays_folder'] + os.sep + str(name)
    if not os.path.exists(path_tmp):
        return 'Data set does not exist'
    elif name == '':
        return 'Bad name'
    else:
        shutil.rmtree(path_tmp)
        update_main_file_of_datasets()
        text = name + ' was deleted'
        log('delete_dataset', text)
        return text


def delete_selection(dataset_id, selection_id):
    """
    Deletes the selection in data set from database.
    :param dataset_id: Name of data set
    :param selection_id: Name of selection
    :return: String with result of deleting
    """
    path_tmp = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    if os.path.exists(path_tmp):
        path_tmp = path_tmp + os.sep + str(selection_id)
        if os.path.exists(path_tmp):
            shutil.rmtree(path_tmp)
            update_selections_info(dataset_id, None)
            text = "selection with ID " + str(selection_id) + " was deleted"
            log('create_selection', text)
            return text
        else:
            text = "Selection with ID " + str(selection_id) + "does not exist"
            return text
    else:
        text = "Dataset with ID " + str(dataset_id) + "does not exist"
        return text


def delete_method(method_id):
    """
    Deletes method from file database.

    :param method_id: Method id in file database.
    :return: True/False
    """
    try:
        path = get_method_path_by_id(method_id)
        shutil.rmtree(path)
    except IOError:
        return False


def delete_all_methods():
    """
    Deletes all methods

    :return: True/False
    """
    try:
        path = get_methods_folder()
        shutil.rmtree(path)
        os.mkdir(path)
        return True
    except:
        return False


def delete_all_tests():
    """
    Delete all tests

    :return: True/False
    """
    try:
        path_res = config['results_folder']
        shutil.rmtree(path_res)
        check_results_folder()
        return True
    except:
        return False


def delete_test(test_order):
    """
    Deletes test from file DB.

    :param test_order: ID of test in file DB
    :return: True/False
    """
    check_results_folder()
    path_res = config['results_folder']
    path_test = path_res + os.sep + str(test_order)
    if os.path.exists(path_test):
        shutil.rmtree(path_test)
        return True
    else:
        return False


# getting methods

def get_molecule_object(dataset_id, molecule_name):
    """
    Returns molecule object.
    :param dataset_id: Data-set where the molecule is from
    :param molecule_name: Molecule name
    :return: RDkit molecule object
    """

    sdf_path = get_path_sdf(dataset_id)
    for mol in Chem.SDMolSupplier(sdf_path):
        if mol is not None and mol.GetProp('_Name') == molecule_name:
            return mol
    return None


def get_method_path_by_id(method_id):
    """
    Returns a path to a place where the required method is stored.

    :param method_id: ID of required method.
    :return: Path to the method.
    """
    methods_folder = get_methods_folder()
    files = os.listdir(methods_folder)
    for file in files:
        if file.find(method_id) > -1:
            return methods_folder + os.sep + file
    raise IOError("The required file wasn't found")


def get_methods_folder():
    """
    Returns path of folder where are saved methods files
    :return: Path of folder where are saved methods files
    """
    path = config['methods_folder']
    if not os.path.exists(path):
        os.makedirs(path)
        init_path = os.path.join(path, "__init__.py")
        with open(init_path, "w"):
            pass
    return path


def get_all_methods_names():
    """
    Returns names of methods in method folder.
    :return: List of names.
    """
    methods_folder = get_methods_folder()
    return os.listdir(methods_folder)


def get_assays():
    """
    Returns all data sets in file database saved on computer.

    :return: List with names of data sets.
    """
    global check_assay
    global assay
    path_tmp = config['assays_folder']
    names = []
    if check_assay:
        files = os.listdir(path_tmp)
        for file in files:
            file_path = os.path.join(path_tmp, file)
            if os.path.isdir(file_path):
                names.append(file)
        check_assay = False
        assay = names
    if assay is not None:
        return assay
    else:
        return []


def get_dataset_selection_number(dataset):
    """
    Returns count of selection in given data set
    :param dataset: Id data-set
    :return: Int
    """

    number = get_safe_id_for_selection(dataset)
    number = int(number) - 1
    return number


def get_safe_id_for_data_set():
    """
    Returns id which can be used for a new created data set
    :return: Number like ID
    """
    path_info = config['assays_info_file']
    with open(path_info, 'r') as file:
        reader = json.load(file)
        if "actual_number" in reader:
            biggest = int(reader["actual_number"])
        else:
            biggest = 0
    return biggest + 1


def valid_data_sets():
    """
    Returns count of valid data sets. Valid data set = has greater than or equal to one selections.
    :return: Number representing count of data sets
    """
    number = 0
    assays = get_assays()
    for a in assays:
        if 0 < len(get_data_set_selections(a)):
            number += 1
    return number


def get_safe_id_for_selection(dataset_id):
    """
    Returns ID which can be used for a new created selection.
    :param dataset_id: name of data set which a new selection will be inserted in
    :return: Number like id
    """
    path_assay = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    path_info = path_assay + os.sep + "info.json"
    with open(path_info, 'r') as file:
        reader = json.load(file)
        if "actual_number" in reader:
            biggest = int(reader["actual_number"])
        else:
            if "selection" in reader and len(reader["selection"]) > 0:
                biggest = len(reader["selection"])
            else:
                biggest = 0
    return biggest + 1


def get_selection_train(dataset_id, selection_id):
    """
    Returns train data of given selection.
    :param dataset_id: ID of data set which a given selection belongs to
    :param selection_id: ID of selection which a train data should be get from
    :return: String if getting data was unsuccessful or list with two elements: list with active molecules and list with inactive molecules.
    """
    path_tmp = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    if os.path.exists(path_tmp):
        path_tmp = path_tmp + os.sep + str(selection_id)
        if os.path.exists(path_tmp):
            path_tmp = path_tmp + os.sep + "info.json"
            with open(path_tmp, 'r') as inp:
                file = json.load(inp)
                active = []
                inactive = []
                for item in file['data']['molecules']['train']['actives']:
                    active.append(item)
                for item in file['data']['molecules']['train']['inactives']:
                    inactive.append(item)
                train = {}
                train['active'] = active
                train['inactive'] = inactive
                return train
        else:
            text = "Selection with ID " + str(selection_id) + " does not exist"
            return text
    else:
        text = "Dataset with ID " + str(dataset_id) + "does not exist"
        return text


def get_selection_test(dataset_id, selection_id):
    """
    Returns test data of given selection.
    :param dataset_id: ID of data set which a given selection belongs to
    :param selection_id: ID of selection which a test data should be get from
    :return: String if getting data was unsuccessful or list with test molecules
    """
    path_tmp = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    if os.path.exists(path_tmp):
        path_tmp = path_tmp + os.sep + str(selection_id)
        if os.path.exists(path_tmp):
            path_tmp = path_tmp + os.sep + "info.json"
            with open(path_tmp, 'r') as inp:
                file = json.load(inp)
                test = []
                for item in file['data']['molecules']['test']:
                    test.append(item)
                return test
        else:
            text = "Selection with ID " + str(selection_id) + " does not exist"
            return text
    else:
        text = "Dataset with ID " + str(dataset_id) + "does not exist"
        return text


def get_selection_valid(dataset_id, selection_id):
    """
        Returns validation data of given selection.

        :param dataset_id: ID of data set which a given selection belongs to
        :param selection_id: ID of selection which a train data should be get from
        :return: String if getting data was unsuccessful or list with two elements: list with active molecules and list with inactive molecules.
        """
    path_tmp = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    if os.path.exists(path_tmp):
        path_tmp = path_tmp + os.sep + str(selection_id)
        if os.path.exists(path_tmp):
            path_tmp = path_tmp + os.sep + "info.json"
            with open(path_tmp, 'r') as inp:
                file = json.load(inp)
                active = []
                inactive = []
                if "validation" in file['data']['molecules']:
                    if "actives" in file['data']['molecules']['validation']:
                        for item in file['data']['molecules']['validation']['actives']:
                            active.append(item)
                    if "inactives" in file['data']['molecules']['validation']:
                        for item in file['data']['molecules']['validation']['inactives']:
                            inactive.append(item)
                validation = {'active': active, 'inactive': inactive}
                return validation
        else:
            text = "Selection with ID " + str(selection_id) + " does not exist"
            return text
    else:
        text = "Dataset with ID " + str(dataset_id) + "does not exist"
        return text


def get_data_set_selections(dataset_id):
    """
    Returns all selections which are created from given data set.
    :param dataset_id: ID of data set which selections should be take from
    :return: List with names of selections
    """
    path_tmp = config['assays_folder'] + os.sep + str(dataset_id) + os.sep + "selection"
    path_info = path_tmp + os.sep + "info.json"
    try:
        with open(path_info, 'r') as inp:
            file = json.load(inp)
            names = []
            for item in file['selection']:
                names.append(item)
            return names
    except:
        return []


def get_metadata(dataset):
    """
    Returns metadata of given data set. Metadata can be different for different data sets.
    Common: dataset_id, date, datasetName, targetName, numberActives, numberInactives
    :param dataset: Id of data set which should metadata should be take from.
    :return: Dictionary with metadata or string informing of failing.
    """
    check_main_database_folder()
    path_tmp = config['assays_folder'] + os.sep + str(dataset) + os.sep + 'info.json'
    if os.path.exists(path_tmp):
        data = {}
        with open(path_tmp, 'r') as inp:
            file = json.load(inp)
            for key in file['metadata']:
                value = file['metadata'][key]
                data[key] = value
            return data
    else:
        return 'Info file can\'t be loaded'


def get_selection_metadata(dataset, selection):
    """
    Returns metadata of given selection. Metadata can be different for different selections.

    :param selection: Id of selection which metadata should be take from.
    :param dataset: Id of data set where the selection is.
    :return: Dictionary with metadata or string informing of failing.
    """
    check_main_database_folder()
    path_tmp = os.path.join(config['assays_folder'], str(dataset), "selection")
    if os.path.exists(path_tmp):
        path_tmp = path_tmp + os.sep + str(selection)
        if os.path.exists(path_tmp):
            path_tmp = os.path.join(path_tmp, "info.json")
            with open(path_tmp, 'r') as inp:
                file = json.load(inp)
                return file['metadata']
        else:
            text = "Selection with ID " + str(selection) + " does not exist"
            return text
    else:
        text = "Dataset with ID " + str(dataset) + "does not exist"
        return text


def get_active(dataset):
    """
    Returns names of active molecules in given data set.

    :param dataset: ID of data set
    :return: List with active molecules or string informing that getting of molecule names failed
    """
    check_main_database_folder()
    path_tmp = config['assays_folder'] + os.sep + str(dataset) + os.sep + 'info.json'
    if os.path.exists(path_tmp):
        active = []
        with open(path_tmp, 'r') as inp:
            file = json.load(inp)
            for line in file['data']['molecules']['active']:
                active.append(line)
            return active
    else:
        return 'Info file can\'t be loaded'


def get_inactive(dataset):
    """
    Returns names of inactive molecules in given data set.
    :param dataset: ID of data set
    :return: List with inactive molecules or string informing that getting of molecule names failed
    """
    check_main_database_folder()
    path_tmp = config['assays_folder'] + os.sep + str(dataset) + os.sep + 'info.json'
    if os.path.exists(path_tmp):
        inactive = []
        with open(path_tmp, 'r') as inp:
            file = json.load(inp)
            for line in file['data']['molecules']['inactive']:
                inactive.append(line)
            return inactive
    else:
        return 'Info file can\'t be loaded'


def get_path_sdf(dataset):
    """
    Returns a path with file containing SDF desription of molecules in given data set.
    :param dataset: Id of data set
    :return: String with path of file or string informing that SDF file couldn't be loaded.
    """
    check_main_database_folder()
    path_tmp = config['assays_folder'] + os.sep + str(dataset) + os.sep + 'mols.sdf'
    if os.path.exists(path_tmp):
        return path_tmp
    else:
        return 'SDF file can\'t be loaded'


def number_selections():
    """
    Returns count of valid selections in file database.
    :return: Count of selections
    """
    number = 0
    assays = get_assays()
    for a in assays:
        number += len(get_data_set_selections(a))
    return number


def get_safe_number_of_test():
    """
    Returns id which can be used for a new created test.
    :return: Number like id
    """
    check_results_folder()
    path_info = config['results_info_file']
    if os.path.exists(path_info):
        with open(path_info, 'r') as file:
            reader = json.load(file)
            return int(reader['number of tests']) + 1


def get_safe_number_of_result():
    """
    Returns safety id which can be used for a new created result.
    :return: Number like id
    """
    check_results_folder()
    path_info = config['results_info_file']
    if os.path.exists(path_info):
        with open(path_info, 'r') as file:
            reader = json.load(file)
            return int(reader['number of results']) + 1


# management of DB


def dataset_exist(data_set_name):
    """
    Returns True if data set in file database exists.
    :param data_set_name: Id of data set.
    :return: True/False
    """
    assays = get_assays()
    if data_set_name in assays:
        return True
    else:
        return False


def selection_exist(data_set_name, selection_name):
    """
    Returns True if the given selection in the file database exists.
    :param data_set_name: Id of data set.
    :param selection_name: Id of selection.
    :return: True/False
    """
    sels = get_data_set_selections(data_set_name)
    if selection_name in sels:
        return True
    else:
        return False


def log(sender, action, level="info"):
    """
    Writes information into logging file.
    :param sender: Function which sent a request for logging
    :param action: Action which occured
    :param level: Level of action
    :return: Nothing
    """
    path_tmp = config['log_file']
    if sender is None:
        s = 'Sender is unknown'
    else:
        s = sender
    if action is None:
        a = 'Action is unknown'
    else:
        a = action
    with open(path_tmp, 'a') as logger:
        tm = str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + ":" + str(
            time.localtime().tm_sec) + "," + str(
            time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(time.localtime().tm_year)
        line = '[' + level.upper() + '] : ' + tm + ' : ' + s + ' : ' + a + '\n'
        logger.write(line)


def clear_log():
    """
    Clear a log file.
    :return: Nothing
    """
    path_tmp = config['log_file']
    os.remove(path_tmp)
    with open(path_tmp, 'w'):
        pass


def check_inner_database(deep_review=True):
    """
    Check if file database is consistent.
    :param deep_review True/ False time-consuming operation - check of SDF, data set and selections are consistent
    :return: Nothing

    """
    # kontrola hlavnich slozek databaze
    check_main_database_folder()
    # aktualizuje hlavni soubor s informacemi o databazi
    update_main_file_of_datasets()
    assays = get_assays()
    for a in assays:
        print("\nChecking dataset {0}\n".format(a))
        # aktualizace souboru s informacemi o vyberech
        update_selections_info(a, None)
        if deep_review:
            active = get_active(a)
            inactive = get_inactive(a)
            molecules = []
            # kontrola molekul a SDF souboru (zda obsahuje vsechny soubory z datasetu)
            sdf_path = get_path_sdf(a)
            for molecule in rdkit.Chem.SDMolSupplier(sdf_path):
                molecules.append(molecule.GetProp('_Name'))
                if molecule.GetProp('_Name') not in active and molecule.GetProp('_Name') not in inactive:
                    print("\n{0} is not in active neither inactive\n".format(molecule.GetProp('_Name')))
            for mol in active:
                if mol not in molecules:
                    print("{0} from actives is not in sdf file\n".format(mol))
            for mol in inactive:
                if mol not in molecules:
                    print("{0} from decoays is not in sdf file\n".format(mol))
            # zkontroluje validnost vyberu
            selections = get_data_set_selections(a)
            for selection in selections:
                print("\nChecking selection {0}\n".format(selection))
                train_sel = get_selection_train(a, selection)
                test_sel = get_selection_test(a, selection)
                act_sel = train_sel['active']
                inact_sel = train_sel['inactive']
                if (len(act_sel) + len(inact_sel)) < 1:
                    print("Few molecules in train data")
                if len(test_sel) < 1:
                    print("Few molecules in test data")
                print("Train data:\n")
                for i in act_sel:
                    if i not in active:
                        print(
                            "Train molecule {0} from selection {1} in dataset {2} is missing in active molecules\n".format(
                                i, selection, a))
                for i in inact_sel:
                    if i not in inactive:
                        print(
                            "Train molecule {0} from selection {1} in dataset {2} is missing in inactive molecules\n".format(
                                i, selection, a))
                print("Test data:\n")
                for i in test_sel:
                    if i not in active and i not in inactive:
                        print("Test molecule {0} from selection {1} in dataset {2} is missing in molecules\n".format(i,
                                                                                                                     selection,
                                                                                                                     a))


def check_results_folder():
    """
    If info file for result doesn't exists, creates a new one.
    :return: Nothing
    """
    path_res = config['results_folder']  # config['database_folder'] + separator + 'results'
    path_info = config['results_info_file']  # pathAs + separator + "info.json"
    if not os.path.exists(path_res):
        os.makedirs(path_res)
    if not os.path.exists(path_info):
        files = os.listdir(path_res)
        names = []
        for file in files:
            pt = path_res + separator + file
            if os.path.isdir(pt):
                names.append(file)
        with open(path_info, 'w') as f:
            json.dump(
                {
                    'date': str(time.localtime().tm_hour) + ":" + str(time.localtime().tm_min) + "," + str(
                        time.localtime().tm_mday) + "." + str(time.localtime().tm_mon) + "." + str(
                        time.localtime().tm_year),
                    'tests': names,
                    'number of tests': len(names),
                    'number of results': 0
                }, f, indent=2
            )


def check_main_database_folder():
    """
    Creates basic folders in file database if they don't exist.
    :return: None
    """
    path_as = config['assays_folder']  # config['database_folder'] + separator + 'assay'
    path_res = config['results_folder']  # config['database_folder'] + separator + 'results'
    path_info = config['assays_info_file']  # path_as + separator + "info.json"
    path_log = config['log_file']  # config['database_folder'] + separator + 'log.txt'
    path_version = config['database_version_file']
    if not os.path.exists(path_as):
        os.makedirs(path_as)
    if not os.path.exists(path_res):
        os.makedirs(path_res)
    if not os.path.exists(path_info):
        with open(path_info, 'w') as file:
            json.dump({}, file, indent=2)
    if not os.path.exists(path_log):
        open(path_log, 'w')
    if not os.path.exists(path_version):
        open(path_version, 'w')


def clear_results_folder():
    """
    Clear folder with results of tests.
    :return: Nothing
    """
    path_res = config['results_folder']  # config['database_folder'] + separator + 'results'
    shutil.rmtree(path_res)
    check_results_folder()


def clear_assays_folder():
    """
    Clear folder with assays.
    :return: Nothing
    """
    path_as = config['assays_folder']
    shutil.rmtree(path_as)
    check_results_folder()


def fill_config(config):
    """
    Fill variable config with basic paths which are used in other methods.
    :param config: Variable which should be filled.
    :return: Filled config variable
    """
    config['temp_folder'] = "../temp"
    config['database_folder'] = "../database"
    config['assays_folder'] = config['database_folder'] + os.sep + 'assays'
    config['results_folder'] = config['database_folder'] + os.sep + 'results'
    config['log_file'] = config['database_folder'] + os.sep + 'log.txt'
    config['assays_info_file'] = config['assays_folder'] + os.sep + "info.json"
    config['results_info_file'] = config['results_folder'] + os.sep + "info.json"
    config['database_version_file'] = config['database_folder'] + separator + "version.txt"
    config['methods_folder'] = config['database_folder'] + os.sep + "methods"
    return config


def is_database_online():
    """
    Check if file database is available.

    :param update_database True if database should be updated if there is a new version.
    :return: True/False if database is alright
    """
    if os.path.exists(config['database_folder']):
        check_main_database_folder()
        if valid_data_sets() < 1:
            return False
        if number_selections() < 1:
            return False
        return True
    else:
        return False


def update_database():
    """
    Downloads or updates a init file database.

    :return:
    """
    import support_scripts.download_start_db as down_db
    down_db.main()


def _read_config(configuration_path):
    """
    Load a configuration for the database to the global variable config.
    :param configuration_path: Path of a json file with a configuration.
    :return: True or false by the success of loading.
    """
    global config
    try:
        with open(configuration_path, "r") as config_file:
            config_ = json.load(config_file)['data_conf']
            config.update(config_)
        return True
    except Exception as e:
        print(e)
        return False


def init(database_configuration_path):
    """
    Initialize database module.
    :param database_configuration_path: Path to json file with configuration for the database.
    :return: True if inizalization was successful or raise an exception.
    """
    global config
    if _read_config(database_configuration_path):
        return True, config
    else:
        sys.tracebacklimit = None
        raise Exception("Database configuration cannot been read")


# first ... load basic paths
config = fill_config({})

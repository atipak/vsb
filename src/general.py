import file_database
import sql_database
import random
import time
import os
import json
import file_formats
import sys


class MyException(Exception):
    """
    A general exception.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class SQLQueryException(Exception):
    """
    A sql exception.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class BadUserInformationException(Exception):
    """
    An exception raised by inserting bad user information.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ConnectingException(Exception):
    """
    An exception for problems with connection.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class RemoteException(Exception):
    """
    An exception for problems by remote connections.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ResultException(Exception):
    """
    An exception raised by problems with inserting or creating result
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InsufficientlySelectionsException(Exception):
    """
    An exception which is raised when there is not enough selection in the database.
    """

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Linux:
    """
    An class representing LINUX OS.
    """
    path_separator = "/"
    command_separator = " ; "

    def ret_remove_rec_com(folder_name):
        """
        Remove all directories and files in desired folder (include the folder).
        :param folder_name Name of folder which should be removed.
        :return: Command
        """
        remove_recursively_com = 'rm -rf '
        return remove_recursively_com + folder_name

    def ret_suspend_commander(command):
        """
        :return: Command in suspend mode.
        """
        return command + " &"


class Windows:
    """
    An class representing WINDOWS OS.
    """
    path_separator = "\\"
    command_separator = " & "

    def ret_remove_rec_com(folder_name):
        """
        Remove all directories and files in desired folder (include the folder).
        :param folder_name Name of folder which should be removed.
        :return: Command
        """
        first_part = 'rmdir '
        second_part = ' /s /q'
        return first_part + folder_name + second_part

    def ret_suspend_commander(command):
        """
        :return: Command in suspend mode.
        """
        return "START /B " + command


successful_results = {}
ended_threads = {}
main_conf_file_path = "../configurations/main_conf.json"


def read_input(file_name=None):
    """
    Reads input for a test from json file.
    :param file_name: File with input.
    :return: List [metadata, tests = dictionary].
    """
    if file_name is not None:
        if os.path.exists(file_name):
            with open(file_name, "r") as data_file:
                inp = json.load(data_file)
                metadata = inp['metadata']
                tests = []
                for name in inp['test']:
                    if name in metadata['ignore']:
                        continue
                    tests.append(inp['test'][name])
                return [metadata, tests]
        else:
            sys.tracebacklimit = None
            raise IOError("Configuration file not found")
    else:
        sys.tracebacklimit = None
        raise Exception("Configuration file wasn't entered")


def verify_user(login):
    """
    Verifies if the username is stored in the database.
    :param login: Username
    :return: id of user or -1 for a bad request
    """
    users = sql_database.get_user(None, login)
    if users is None or len(users) == 0:
        return -1
    else:
        return users[0][0]


def check_user_selections(selections):
    """
    Checks if given selections from user are valid.
    :param selections: Dictionary with keys 'dataset' and 'selection'
    :return: valid selections
    """
    all_selections = False
    select = []
    assays = file_database.get_assays()
    for sel in selections:
        if "dataset" not in sel:
            continue
        if sel['dataset'] not in assays:
            print('Dataset ', sel['dataset'], ' not found')
            continue
        else:
            sels = file_database.get_data_set_selections(sel['dataset'])
            if "selection" not in sel:
                all_selections = True
                for s in sels:
                    select.append({'dataset': sel['dataset'], 'selection': s})
                break
            if sel['selection'] not in sels:
                print('Selection {0} in data set {1} not found'.format(sel['dataset'], sel['selection']))
                continue
            else:
                print('Selection {0} from data set {1} was added'.format(sel['dataset'], sel['selection']))
                select.append({'dataset': sel['dataset'], 'selection': sel['selection']})
    return select, all_selections


def choose_selection(assays, select):
    """
    Choose selections randomly
    :param assays: Data sets
    :param select: Already chosen
    :return: Dictionary with keys "dataset" and "selection" representing one selection
    """
    cont = True
    while cont:
        cont = False
        if len(assays) > 1:
            d = random.randint(0, len(assays) - 1)
            dataset = assays[d]
        elif len(assays) == 1:
            dataset = assays[0]
        else:
            sys.tracebacklimit = None
            raise InsufficientlySelectionsException("Not enough data sets")
        selections = file_database.get_data_set_selections(dataset)
        if 1 < len(selections):
            num = random.randint(0, len(selections) - 1)
            selection = selections[num]
        elif 1 == len(selections):
            selection = selections[0]
        else:
            cont = True
            continue
        for s in select:
            if s['dataset'] == dataset and s['selection'] == selection:
                cont = True
                break
    return {'dataset': dataset, 'selection': selection}


def check_execute_command(config):
    """
    Add execute command into configuration.
    :param config: Test configuration
    :return: New test configuration
    """
    if config['typefile'] == "general":
        files = os.listdir(config['file_method_path'])
        if "method_configuration.json" not in files:
            print("File 'method_configuration.json' in {0} wasn't found. Application was stopped.".format(
                config['file_method_path']))
            exit(13)
        else:
            con_path = os.path.join(config['file_method_path'], "method_configuration.json")
            with open(con_path) as stream:
                c = json.load(stream)
                config['execute_command'] = c['execute_command']
            return config
    else:
        return config


def choose_selections(repeat, user_choice):
    """
    Chooses required number of selections.
    :param repeat: Count of selections
    :param user_choice: Already chosen selections by user
    :return: List with dictionaries representing selection with keys "dataset" and "selection"
    """
    assays = file_database.get_assays()
    # nacist dalsi selections
    for i in range(repeat):
        user_choice.append(choose_selection(assays, user_choice))
    return user_choice


def choose_selections_after(repeat, selected):
    """
    Chooses required number of selections
    :param repeat: Count of selections
    :param selected: Already chosen selections in previous iterations
    :return: List of dictionaries with keys "dataset" and "selection"
    """
    new_select = []
    assays = file_database.get_assays()
    max = len(selected) + repeat
    if max < file_database.number_selections():
        # nacist dalsi selections
        for i in range(0, repeat):
            sel = choose_selection(assays, selected)
            new_select.append(sel)
            selected.append(sel)
        return {'new_selected': new_select, 'all': selected}
    else:
        # vratit dovolene maximum
        maximum = file_database.number_selections() - len(selected)
        if maximum == 0:
            sys.tracebacklimit = None
            raise InsufficientlySelectionsException("Insufficiently selections in DB.")
        for i in range(0, file_database.number_selections() - len(selected)):
            sel = choose_selection(assays, selected)
            new_select.append(sel)
            selected.append(sel)
        return {'new_selected': new_select, 'all': selected}


def dbs_initialization(mode, init_config_path=None):
    """
    Check and initialize SQL and data set databases.

    :param mode: Mode which application is launched in
    :param init_config_path: Path to a file with base configuration of app. None means default localization.
    :return: It could raise a ConnectingException or IOError
    """
    # default file path
    if init_config_path is not None:
        if not os.path.exists(init_config_path):
            text = "File {} doesn't exist.".format(init_config_path)
            raise IOError(text)
        path = init_config_path
    else:
        if not os.path.exists(main_conf_file_path):
            text = "File {} doesn't exist.".format(main_conf_file_path)
            raise IOError(text)
        path = main_conf_file_path
    set_config_path(main_conf_file_path)
    prepare_databases(path)
    online, exception = sql_database.is_sql_online()
    if not online:
        sys.tracebacklimit = None
        raise ConnectingException("A problem with connecting to SQL database: {0}".format(exception))
    else:
        print("SQL database check")
        sql_database.init_sqldatabase(False, False, False, False, False, False, False)
        ms = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name, None, 0, None)
        if len(ms) < 1:
            start_init_methods()
        us = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableUs.table_name,
                                             sql_database.DatabaseHelper.Clauses.Where.User.by_id(1), 0, None)
        if us is not None and len(us) == 0:
            sql_database.add_user_to_db("host")
    if file_database.is_database_online():
        print("File database check")
    else:
        raise ConnectingException("A problem with local data set database. No data-sets avaible")


k = 0


def get_id_of_method(method_name):
    """
    Return ID of method for its saving and its identification.
    :param method_name: Name of method
    :return: Method id
    """
    global k
    k += 1
    if 3 < len(method_name):
        return method_name[:4] + "_" + str(time.time()).replace(".", "_") + "m" + str(k)
    else:
        return method_name + "_" + str(time.time()).replace(".", "_") + "m" + str(k)


def check_temp_folder():
    """
    Check if temp folder exist else create new
    :return: Nothing
    """
    if not os.path.exists(get_temp_folder_path()):
        os.mkdir(get_temp_folder_path())


def get_os_class(OS_name):
    """
    Returns class representing OS.
    :param OS_name: LINUX/WINDOWS
    :return: Representing class
    """
    if OS_name == "LINUX":
        return Linux
    if OS_name == "WINDOWS":
        return Windows


def check_user(config):
    """
    Checks if user id is given in the input else set his id on 1 (host)
    :param config: Input configuration
    :return: New input configuration
    """
    if 'user' not in config:
        print("Field user not found, therefore user is set to 1(guest)")
        config['user'] = 1
    return config


def get_selections(config):
    """
    Returns required number of selections
    :param config: Input configuration
    :return: List of selections.
    """
    selections = []
    if 'd/s' in config:
        selections = config['d/s']
    else:
        print("User defined selections weren't found")
        selections = []
    select, all_selections = check_user_selections(selections)
    if not all_selections:
        config['all_selections'] = False
        rest = int(config['repeat']) - len(select)
        if rest > 0:
            select = choose_selections(rest, select)
            return select, config
        # some selections can be inserted more times therefore I take only first i selections entered by user
        else:
            temp_select = []
            for i in range(int(config['repeat'])):
                temp_select.append(select[i])
            return temp_select, config
    else:
        config['repeat'] = len(select)
        config['all_selections'] = True
        return select, config


def check_repeat(config):
    """
    Checks if repeat in input is valid
    :param config: Input configuration
    :return: Repair version of input configuration
    """
    number_selections = file_database.number_selections()
    # set max on three quarter of count of selections
    if not int(config['repeat']) < number_selections:
        set_max = int(3 * number_selections / 4)
        print("Too many repeats")
        print("Number of repeating is set to {0}".format(set_max))
        config['repeat'] = set_max
    return config


def add_methods_to_db(config, test_path=None):
    """
    Add methods file into databases.
    :param config:  Input configuration
    :param test_path: Path which is connected with test in the database.
    :return: Serial number of record in the database. -1 if there is with number something wrong
    """
    if not sys.path[0] == os.path.abspath(file_database.get_methods_folder()):
        sys.path.insert(0, os.path.abspath(file_database.get_methods_folder()))

    # method is in DB, it is not a new method if its name starts with &
    py_version = False
    # add to file database
    if 'repr_method' in config:
        if str(config['repr_method']).strip().startswith("&"):
            try:
                # get information about method
                result_set = sql_database.get_methods_names(str(config['repr_method']).strip()[1:])
                method_type = result_set[0][1]
                if not method_type == "py":
                    sys.tracebacklimit = None
                    raise SQLQueryException("Number of record in SQL DB corresponds to general file")
                else:
                    id = result_set[0][2]
                    name = result_set[0][4]
                    config['repr_method_path'] = file_database.get_method_path_by_id(id)
                    config['repr_method_name'] = name
            except:
                raise IOError("ID wasn't found")
        else:
            path = str(config['repr_method']).strip()
            if not os.path.isdir(path):
                raise IOError("Representation function path is not a dir")
            config['repr_method_name'] = os.path.basename(path)
            if not os.path.exists(path):
                IOError("Representation method doesn't exist.")
            r_value, config['repr_method_path'] = file_database.add_method_file_new_version(
                path, get_id_of_method(config['repr_method_name']))
        # sys.path.insert(0,config['repr_method_path'])
        py_version = True
    if 'sim_method' in config:
        if str(config['sim_method']).strip().startswith("&"):
            try:
                result_set = sql_database.get_methods_names(str(config['sim_method']).strip()[1:])
                method_type = result_set[0][1]
                if not method_type == "py":
                    sys.tracebacklimit = None
                    raise SQLQueryException("Number of record in SQL DB corresponds to general file")
                else:
                    id = result_set[0][3]
                    name = result_set[0][5]
                    config['sim_method_path'] = file_database.get_method_path_by_id(id)
                    config['sim_method_name'] = name
            except:
                raise IOError("ID wasn't found")
        else:
            path = str(config['sim_method']).strip()
            if not os.path.isdir(path):
                raise IOError("Similarity function path is not a dir")
            config['sim_method_name'] = os.path.basename(path)
            if not os.path.exists(path):
                IOError("Similarity method doesn't exist.")
            r_value, config['sim_method_path'] = file_database.add_method_file_new_version(
                path, get_id_of_method(os.path.basename(path)))
            # sys.path.insert(0, config['sim_method_path'])
    if 'file' in config:
        if str(config['file']).strip().startswith("&"):
            try:
                result_set = sql_database.get_methods_names(str(config['file']).strip()[1:])
                method_type = result_set[0][1]
                if not method_type == "general":
                    sys.tracebacklimit = None
                    raise SQLQueryException("Number of record in SQL DB corresponds to method files")
                else:
                    id = result_set[0][2]
                    name = result_set[0][4]
                    config['file_method_path'] = file_database.get_method_path_by_id(id)
                    config['file_method_name'] = name
            except:
                raise IOError("ID wasn't found")
        else:
            config['file_method_name'] = os.path.basename(config['file'])
            if not os.path.isdir(config['file']):
                raise IOError("Method path is not a dir")
            r_value, config['file_method_path'] = file_database.add_method_file_new_version(
                config['file'], get_id_of_method(config['file_method_name']))
            # sys.path.insert(0, config['file_method_path'])
    # add to SQL database if is needed
    if py_version:
        if str(config['sim_method']).startswith("&") or str(config['repr_method']).startswith("&"):
            if str(config['sim_method']).strip() == str(config['repr_method']).strip():
                number = str(config['sim_method']).strip()[1:]
            else:
                if str(config['sim_method']).startswith("&") and str(config['repr_method']).startswith("&"):
                    ids = str(config['repr_method']).strip()[1:] + "#" + str(config['sim_method']).strip()[1:]
                    existence = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name,
                                                                sql_database.DatabaseHelper.Clauses.Where.Method.by_join_ids(
                                                                    ids),
                                                                0)
                    if len(existence) > 0:
                        number = existence[0][0]
                    else:
                        number = sql_database.add_method_to_db("py", os.path.basename(config['repr_method_path']),
                                                               os.path.basename(config['sim_method_path']),
                                                               config['repr_method_name'],
                                                               config['sim_method_name'], ids)
                else:
                    if str(config['sim_method']).startswith("&"):
                        # add repr function
                        number_1 = sql_database.add_method_to_db("py", os.path.basename(config['repr_method_path']),
                                                                 "Empty", config['repr_method_name'], "Empty")
                        number_2 = str(config['sim_method']).strip()[1:]
                        pass
                    else:
                        # add sim function
                        number_2 = sql_database.add_method_to_db("py", "Empty",
                                                                 os.path.basename(config['sim_method_path']),
                                                                 "Empty", config['sim_method_name'])
                        number_1 = str(config['repr_method']).strip()[1:]
                        pass
                    ids = str(number_1) + "#" + str(number_2)
                    number = sql_database.add_method_to_db("py", os.path.basename(config['repr_method_path']),
                                                           os.path.basename(config['sim_method_path']),
                                                           config['repr_method_name'],
                                                           config['sim_method_name'], ids)
        else:
            # add repr function
            number_1 = sql_database.add_method_to_db("py", os.path.basename(config['repr_method_path']),
                                                     "Empty", config['repr_method_name'], "Empty")
            # add sim function
            number_2 = sql_database.add_method_to_db("py", "Empty", os.path.basename(config['sim_method_path']),
                                                     "Empty", config['sim_method_name'])
            ids = str(number_1) + "#" + str(number_2)
            number = sql_database.add_method_to_db("py", os.path.basename(config['repr_method_path']),
                                                   os.path.basename(config['sim_method_path']),
                                                   config['repr_method_name'],
                                                   config['sim_method_name'], ids)
    else:
        if str(config['file']).startswith("&"):
            number = str(config['file']).strip()[1:]
        else:
            number = sql_database.add_method_to_db("general", os.path.basename(config['file_method_path']),
                                                   first_origin_name=config['file_method_name'])
    try:
        number = int(number)
    except Exception as e:
        print(e)
    if isinstance(number, int):
        return number
    else:
        print(number)
        return -1


def get_temp_folder_path():
    """
    Returns a path of the temp folder.

    :return: The path
    """
    return "../temp"


def check_selection(id_dataset, resource):
    """
    Checks importing selection if it is right and everything agrees

    :param id_dataset: Name of data set
    :param resource: List with elements[metadata, train[active][inactive], test molecules]
    :return: True or False by the result of test
    """

    assays = file_database.get_assays()
    if id_dataset not in assays:
        print("ID data set is not in data set DB")
        return False
    else:
        active = file_database.get_active(id_dataset)
        inactive = file_database.get_inactive(id_dataset)
        molecules = list(set(active).union(inactive))
        molecules.sort()
        sel_molecules = list(resource["train_active"] + resource["train_inactive"] + resource["validation_active"] + resource["validation_inactive"] + resource["test"])
        sel_molecules.sort()
        i = 0
        l = 0
        for m_name in sel_molecules:
            while not m_name == molecules[i]:
                i += 1
                if i == len(molecules):
                    i -= 1
                    break
            if m_name == molecules[i]:
                l += 1
            else:
                print("Molecule with name {} wasn't found in sdf description.".format(m_name))
                return False, None
        if l == len(sel_molecules):
            return True, [resource["train_active"], resource["train_inactive"], resource["validation_active"], resource["validation_inactive"], resource["test"]]
        return False, None


def check_dataset(resource, path_sdf):
    """
    Checks importing dataset if it is right and everything agrees

    :param resource: List with elements [active molecules, inactive molecules, metadata, string containing SDF description]
    :param path_sdf: Path to a file with SDF description of molecules
    :return:
    """
    try:
        from rdkit import Chem
        if os.path.exists(path_sdf):
            mols = Chem.SDMolSupplier(path_sdf)
        elif isinstance(path_sdf, str):
            tmp_file_path = get_temp_folder_path()
            tmp_file_path = os.path.join(tmp_file_path, "sdf_tmp_file.sdf")
            with open(tmp_file_path, "w") as file:
                file.write(path_sdf)
            mols = Chem.SDMolSupplier(path_sdf)
        else:
            return False, None
        mol_names = []
        for mol in mols:
            mol_names.append(mol.GetProp('_Name'))
        all_dataset_molecules = list(resource[0] + resource[1])
        all_dataset_molecules.sort()
        mol_names.sort()
        i = 0
        l = 0
        for m_name in all_dataset_molecules:
            while not m_name == mol_names[i]:
                i += 1
                if i == len(mol_names):
                    i -= 1
                    break
            if m_name == mol_names[i]:
                l += 1
            else:
                print("Molecule with name {} wasn't found in sdf description.".format(m_name))
                return False, None
        if l == len(all_dataset_molecules):
            return True, [resource[0], resource[1]]
        return False, None
    except ImportError:
        print("You need module chem")
        return False, None


def load_selection(id_dataset, path_to_selection):
    """
    Imports a selection from file.
    :param id_dataset: Name of data set
    :param path_to_selection:  Path to a file where the selection is saved.
    :return: Two values: Success of importing (True/False) and id of inserted selection. -1 if it wasn't successful
    """
    if os.path.exists(path_to_selection):
        res = file_formats.import_selection(path_to_selection)
        if res is not None:
            b_value, r_value = check_selection(id_dataset, res)
            if b_value:
                name, value = file_database.add_selection(id_dataset, res['metadata'], res['train_active'],
                                                          res['train_inactive'],
                                                          res['test'], res['validation_active'],
                                                          res['validation_inactive'])
                return value, name
    return False, -1


def load_dataset(path_to_dataset, path_sdf):
    """
    Imports a data set from file.
    :param path_to_dataset: Name of file where the data set is saved.
    :param path_sdf: Name of file with SDF information about molecules in data set.
    :return: Id of imported data set.
    """
    if os.path.exists(path_to_dataset):
        res = file_formats.import_dataset(path_to_dataset, path_sdf)
        if res is not None:
            b_value, r_value = check_dataset(res, path_sdf)
            if b_value:
                name = os.path.basename(path_to_dataset)
                return file_database.add_data_set(res[0], res[1], res[2], res[3], name, True)


def clean_temp_folder():
    """
    Removes files from temp folder.
    :return: Nothing
    """
    check_temp_folder()
    import shutil
    try:
        shutil.rmtree(get_temp_folder_path())
    except Exception as e:
        print(e)


def active_processes(procs):
    """
    Counts number of running subprocess which are not daemon.
    :param procs: Old list of subprocess.
    :return: Two values: New list of running subprocess, number of running subprocess.
    """
    act_procs = len(procs)
    n_procs = []
    for p in procs:
        if p.exitcode is None:
            n_procs.append(p)
        else:
            act_procs -= 1
    return n_procs, act_procs


def start_and_check_test(config, test_type):
    """
    Tests input configuration if is valid, insert methods into database, prepare support things for test.

    :param config: Input configuration
    :param test_type: Type of test (parallel, metacentrum, ... )
    :return: a repaired configuration, selected selections, id of test in SQL database
    """
    # nastaveni selections
    select, config = get_selections(config)
    # user
    config = check_user(config)
    # kontrola moznosti uskutecneni zopakovani pokusu
    config = check_repeat(config)
    recid_methods = add_methods_to_db(config)
    # check execute command
    config = check_execute_command(config)
    import datetime
    dt_now = datetime.datetime.now()
    date = str(dt_now.year) + "-" + str(dt_now.month) + "-" + str(dt_now.day)
    if "details" in config:
        details = config['details']
    else:
        details = None
    recid_test = sql_database.add_test_to_db(config['test_id'], config['user'], test_type, config['repeat'],
                                             date, recid_methods, details)
    config['test_id'] = recid_test
    return config, select, recid_test


def id_for_test(config):
    """
    Adds into conffiguration dictionary keys test_id (for safe ID for test), test_path ( a path to a folder associated with the test).
    :param config: Configuration dictionary
    :return: New configuration dictionary
    """
    safe_test_number = int(file_database.get_safe_number_of_test())
    print("Number of test is {0}".format(safe_test_number))
    test_path = file_database.new_test(safe_test_number)
    first_result = 0
    config['test_id'] = safe_test_number
    config['test_path'] = test_path
    config['first_result'] = first_result
    return config


def insert_data_into_sql_database(temp_result, id_test, file_type, dataset, selection, test_path, id_result, recid_test,
                                  runtime=0):
    """
    General method for inserting result data into SQL database.

    :param recid_test: SQL ID of test
    :param temp_result: File with ordered molecules by testing method.
    :param id_test: ID of test in file database.
    :param file_type: Type of testing method (Python or general method)
    :param dataset: Name of data set
    :param selection: Name of selection
    :param test_path: Path to the folder determined for test
    :param id_result: ID of result.
    :param
    :return: Success of inserting.
    """
    import analysis
    active = file_database.get_active(dataset)
    inactive = file_database.get_inactive(dataset)
    if os.path.exists(temp_result):
        # reading molecules from result file
        molecules = []
        print("Reading of molecules")
        with open(temp_result) as f:
            reader = json.load(f)
            for item in reader['data']:
                molecules.append(item)
        # creating analysis and a new file containing it
        print("Doing analysis")
        base_temp_dir = os.path.join(get_temp_folder_path(), str(id_test))
        temp_analysis_file_path = os.path.join(base_temp_dir, str("analyze_{0}.json").format(id_result))
        if not os.path.exists(base_temp_dir):
            os.makedirs(base_temp_dir)
        result_of_analyze = analysis.analyze(molecules, active, inactive, temp_analysis_file_path)
        # inserting part
        try:
            print("Inserting of the subtest result")
            # inserting a record about subtest into database
            recid_subtest = sql_database.add_subtest_to_db(recid_test, runtime, dataset,
                                                           selection)
            # inserting a record about analysis into database
            print("Insert analyses:")
            sql_database.add_analyze_to_db(recid_subtest, result_of_analyze['AUC'], result_of_analyze['EF0.01'],
                                           result_of_analyze['EF0.02'], result_of_analyze['EF0.05'],
                                           result_of_analyze['EF0.005'])
            # inserting a records about molecules
            print("Inserting of molecules results")
            sql_database.add_mols_to_db(recid_subtest, result_of_analyze['scores'])
            # Moving files
            print("Moving files from temporary locations to file database")
            new_result_file_path = file_database.add_result_file(temp_result, test_path, id_result)
            new_analysis_file_path = file_database.add_analysis_file(temp_analysis_file_path, test_path, id_result)
            # Inserting references on methods into the SQL database
            print("Inserting references on methods into the SQL database.")
            if file_type == "python":
                sql_database.add_files_to_db(recid_subtest, new_result_file_path, new_analysis_file_path)
            elif file_type == "general":
                sql_database.add_files_to_db(recid_subtest, new_result_file_path, new_analysis_file_path)
            print("Inserting finished successful")
            return True
        except Exception as e:
            print("Problem in SQL database: ", e)
            return False
    else:
        print('Result file wasn\'t created')
        return False


def prepare_databases(config_file_path):
    """
    Initialize file and SQL databases.

    :param config_file_path: Path to configuration file
    :return: Nothing
    """
    print("SQL database initialization for process")
    sql_ret_value = sql_database.init(config_file_path)
    print("File database initialization for process")
    file_ret_value = file_database.init(config_file_path)


def set_config_path(configuration_path):
    """
    Sets variable main_conf_file_path like configuration_path.

    :param configuration_path: A path to a configuration file.
    :return:
    """
    global main_conf_file_path
    main_conf_file_path = configuration_path


def get_config_path():
    """
    Gets variable main_conf_file_path.

    :return: context of variable main_conf_file_path
    """
    global main_conf_file_path
    return main_conf_file_path


def start_init_methods():
    """
    Puts into database basic methods.
    :return:
    """

    # from test_func
    in_dict = {
        "Atom Pair Fingerprint (l)": "Fingerprint Similarity (s)",
        "Atom Pair Fingerprints Opt (l)": "Fingerprint Similarity Opt (s)",
        "Atoms count (l)": "Euclide metric (s)",
        "Just Do (l)": "Just Do (s)",
        "MACCS Keys Fingerprint (l)": "Braun Blanquet Similarity (s)",
        "MACCS Keys Fingerprint Opt (l)": "Braun Blanquet Similarity Opt (s)",
        "Morgan Fingerprints (l)": "Tanimoto Similarity (s)",
        "Morgan Fingerprints Opt (l)": "Tanimoto Similarity Opt (s)",
        "Topological Torsion Fingerprint (l)": "Dice Similarity (s)",
        "Topological Torsion Fingerprint Opt (l)": "Dice Similarity Opt (s)"
    }
    test_func_path = "../test_func"
    files = os.listdir(test_func_path)
    for file in files:
        if file.endswith("(l)"):
            if file in in_dict:
                complement = in_dict[file]
                file_path = os.path.join(test_func_path, file)
                complement_path = os.path.join(test_func_path, complement)
                data = {"repr_method": file_path,
                        "sim_method": complement_path}
                add_methods_to_db(data)
            else:
                file_path = os.path.join(test_func_path, file)
                path = str(file_path).strip()
                if not os.path.isdir(path):
                    raise IOError("Representation function path is not a dir")
                repr_name = os.path.basename(path)
                if not os.path.exists(path):
                    IOError("Representation method doesn't exist.")
                r_value, repr_path = file_database.add_method_file_new_version(
                    path, get_id_of_method(repr_name))
                number_1 = sql_database.add_method_to_db("py", os.path.basename(repr_path),
                                                         "Empty", repr_name, "Empty")
        elif file.endswith("(g)"):
            file_path = os.path.join(test_func_path, file)
            data = {"file": file_path}
            add_methods_to_db(data)


def get_max_repeat(config):
    """
    Returns max repetitions.

    :param config:
    :return:
    """
    if config["all_selections"]:
        return config['repeat']
    else:
        return 2 * config['repeat']


def actual_done(config, ended, successful):
    """
    Returns actual number of ended test by config all_selections.

    :param config: Configuration
    :param ended: Ended tests
    :param successful: Successful tests.
    :return: int
    """
    if config["all_selections"]:
        return int(ended)
    else:
        return int(successful)


def delete_method(method_id):
    """
    Deletes method from database.

    :param method_id: Method SQL ID
    :return: True/False
    """

    # get method
    method = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name,
                                             sql_database.DatabaseHelper.Clauses.Where.Method.by_id(method_id), 0)
    if method is None or len(method) == 0:
        return False
    method = method[0]
    if method[1] == "general":
        if not file_database.delete_method(str(method[2])):
            return False
        sql_database.delete_method_from_db(str(method[0]))
        return True
    else:
        if not str(method[2]).strip().lower() == "empty" and not str(method[3]).strip().lower() == "empty":
            sql_database.delete_method_from_db(str(method[0]))
            return True
        else:
            ms = []
            # type = True if method is scoring
            if str(method[2]).strip().lower() == "empty":
                type = True
            else:
                type = False
            methods = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name,
                                                      " {0} IS NOT NULL".format(
                                                          sql_database.DatabaseHelper.TableMethod.join_ids), 0)
            for m in methods:
                jids = str(m[6]).split("#")
                if type:
                    if int(jids[1]) == method_id:
                        ms.append(m)
                else:
                    if int(jids[0]) == method_id:
                        ms.append(m)
            # removing methods from ms
            if not file_database.delete_method(str(method[2])):
                return False
            for m in ms:
                sql_database.delete_method_from_db(str(m[0]))


def delete_test(test_id):
    """
    Deletes test from database.
    :param test_id: SQL ID of test.
    :return: True/False
    """
    test = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableTest.table_name,
                                           sql_database.DatabaseHelper.Clauses.Where.Test.by_id(test_id), 0)
    if test is None or len(test) == 0:
        return False
    test_order = test[0][6]
    sql_database.delete_test_from_db(test_id)
    return file_database.delete_test(test_order)


def delete_data_set(dataset_name):
    """
    Deletes data-sets from file database.

    :param dataset_name: Name of data-set.
    :return: True/False
    """
    if dataset_name is None or str(dataset_name).strip() == "":
        print("Bad name")
        return False
    # read all subtests from database
    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(dataset_name)
    vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name,
                                               where, 0)
    for vys in vysledek:
        sql_database.delete_subtest_from_db(vys[0])
    file_database.delete_dataset(dataset_name)
    return True


def delete_selection(dataset_name, selection_id):
    """
    Deletes selection from database.

    :param selection_id: Selection id
    :return: True/False
    """
    if selection_id is None or str(selection_id).strip() == "":
        print("Bad selection id")
        return False
    if dataset_name is None or str(dataset_name).strip() == "":
        print("Bad data-set name")
        return False
    # read all subtests from database
    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_selection(selection_id)
    vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name,
                                               where, 0)
    for vys in vysledek:
        if vys[2] == dataset_name:
            sql_database.delete_subtest_from_db(vys[0])
    file_database.delete_selection(dataset_name, selection_id)
    return True


def safe_copytree(src, dist):
    import shutil
    if os.path.exists(dist):
        shutil.rmtree(dist)
    shutil.copytree(src, dist)


def delete_all_methods():
    """
    Deletes all methods

    :return: True/False
    """
    return file_database.delete_all_methods()


def delete_all_tests():
    """
    Delete all tests

    :return: True/False
    """
    return file_database.delete_all_tests()

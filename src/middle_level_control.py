from general import *
import os
import rdkit.Chem.Draw as draw
import analysis


def database_actions(action_params, file_control):
    if 'task_subtype' not in action_params:
        return
    if file_control and 'output_file' not in action_params:
        return
    elif file_control:
        path = action_params['output_file']
        if not os.path.exists(path):
            path_dir = os.path.dirname(path)
            if not os.path.exists(path_dir):
                os.makedirs(path_dir)
    try:
        if action_params['task_type'] == 'put':
            if action_params['task_subtype'] == "selection":

                if 'id_dataset' not in action_params or 'data_file' not in action_params:
                    data = "Element id_dataset or data_file wasn't found.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                if not os.path.exists(action_params['data_file']):
                    data = "Path written in element data_file is wrong.\n"
                    print("File not found, selection cannot be loaded.")
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                else:
                    ret_value, id_sel = load_selection(action_params['id_dataset'], action_params['data_file'])
                    if ret_value:
                        data = "Loading was successful. Selection from data set {0} was added under id {1}\n".format(
                            action_params['id_dataset'], id_sel)
                        if file_control:
                            file_formats.write_query_result([data], "", path, ret_value)
                        else:
                            return data, ret_value
                    else:
                        data = "Selection couldn't be added\n"
                        if file_control:
                            file_formats.write_query_result([data], "", path, False)
                        else:
                            return data
            elif action_params['task_subtype'] == "dataset":

                if 'sdf_file' not in action_params or 'data_file' not in action_params:
                    data = "Element sdf_file or data_file wasn't found.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                if not os.path.exists(action_params['data_file']) or not os.path.exists(action_params['sdf_file']):
                    data = "Data-set could not be inserted, the SDF file path or the data-set path is wrong.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                else:
                    name, b_value = load_dataset(action_params['data_file'], action_params['sdf_file'])
                    text = name
                    if file_control:
                        file_formats.write_query_result([text], "", path, b_value)
                    else:
                        return text, b_value
            elif action_params['task_subtype'] == "user":

                data = []
                if 'username' not in action_params:
                    data = "Element username wasn't found.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                if 'information' not in action_params:
                    action_params['information'] = ""
                    data.append("Element information wasn't found, column information will be set on empty string.\n")
                result = sql_database.get_user(None, action_params['username'])
                if not len(result) == 0:
                    data.append("User with username {0} exists".format(action_params['login']))
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                id = sql_database.add_user_to_db(action_params['username'], action_params['information'])
                data.append(id)
                if file_control:
                    file_formats.write_query_result([data], "", path, True)
                else:
                    return id
            elif action_params['task_subtype'] == "method":

                data = []
                if "method_type" not in action_params or "file_path" not in action_params:
                    data.append("Key method_type or file_path is missing")
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                if not action_params['method_type'] == "py" and not action_params['method_type'] == "general":
                    data.append("Value of method_type has to be py or general")
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                if action_params['method_type'] == "py" and "py_m_type_option" not in action_params and \
                        not action_params['py_m_type_option'] == "repr" and not action_params["py_m_type_option"] == "sim":
                    data.append("Key py_m_type_option has to be in options and its value has to be repr or sim")
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                r_value, new_path = file_database.add_method_file_new_version(action_params["file_path"],
                                                                              get_id_of_method(
                                                                                  os.path.basename(
                                                                                      action_params["file_path"])))
                if action_params['method_type'] == "py":
                    if action_params['py_m_type_option'] == "repr":
                        id = sql_database.add_method_to_db("py", os.path.basename(new_path), "Empty",
                                                           os.path.basename(action_params["file_path"]), "Empty")
                    else:
                        id = sql_database.add_method_to_db("py", "Empty", os.path.basename(new_path), "Empty",
                                                           os.path.basename(action_params["file_path"]))
                else:
                    id = sql_database.add_method_to_db("general", os.path.basename(new_path), "Empty",
                                                       os.path.basename(action_params["file_path"]), "Empty")
                if file_control:
                    file_formats.write_query_result([data.append(id)], "", path, True)
                else:
                    return id
            else:
                data = "Task type {0} wasn't recognized \n".format(action_params["task_type"])
                if file_control:
                    file_formats.write_query_result([data], "", path, False)
                    return
                else:
                    return data
        elif action_params['task_type'] == 'delete':
            if action_params['task_subtype'] == "selection":

                if 'selection_id' not in action_params or 'data_set_name' not in action_params:
                    data = "Element selection_id or data_set_name wasn't found.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                else:
                    ret_value = delete_selection(action_params['data_set_name'], action_params['selection_id'])
                    if ret_value:
                        data = "Deleting was successful. Selection under id {1} from data set {0} was deleted \n".format(
                            action_params['data_set_name'], action_params['selection_id'])
                        if file_control:
                            file_formats.write_query_result([data], "", path, True)
                        else:
                            return ret_value
                    else:
                        data = "Selection couldn't be deleted\n"
                        if file_control:
                            file_formats.write_query_result([data], "", path, False)
                        else:
                            return data
            elif action_params['task_subtype'] == "dataset":

                if 'dataset_name' not in action_params:
                    data = "Element dataset_name wasn't found.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                else:
                    r_value = delete_data_set(action_params['dataset_name'])
                    if file_control:
                        file_formats.write_query_result([r_value], "", path, True)
                    else:
                        return r_value
            elif action_params['task_subtype'] == "test":
                if 'test_id' not in action_params:
                    data = "Element test_id wasn't found.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                result = delete_test(action_params['test_id'])
                if file_control:
                    file_formats.write_query_result([result], "", path, True)
                else:
                    return result
            elif action_params['task_subtype'] == "method":
                data = []
                if "method_id" not in action_params:
                    data.append("Key method_id is missing")
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                r_value = delete_method(action_params['method_id'])
                if file_control:
                    file_formats.write_query_result([data.append(r_value)], "", path, True)
                else:
                    return r_value
            else:
                data = "Task type {0} wasn't recognized \n".format(action_params["task_type"])
                if file_control:
                    file_formats.write_query_result([data], "", path, False)
                    return
                else:
                    return data
        elif action_params['task_type'] == 'get':
            if action_params['task_subtype'] == "test":

                if "full_info" in action_params:
                    if "test_id" not in action_params:
                        data = "Test ID not found"
                        if file_control:
                            file_formats.write_query_result([data], "", path, False)
                            return
                        else:
                            return data
                    if file_control:
                        write_info_about_test(action_params["test_id"], path, True)
                        return
                    else:
                        return write_info_about_test(action_params["test_id"], "", False)
                if "all" in action_params:
                    vysledek = sql_database.read_from_database(
                        sql_database.DatabaseHelper.Clauses.Special.tests_basic_info.what(),
                        sql_database.DatabaseHelper.TableTest.table_name,
                        None,
                        0,
                        sql_database.DatabaseHelper.Clauses.Special.tests_basic_info.join())
                    if file_control:
                        file_formats.write_query_result(vysledek,
                                                        sql_database.DatabaseHelper.Clauses.Special.tests_basic_info.column_names(),
                                                        path, True)
                    else:
                        return vysledek, sql_database.DatabaseHelper.Clauses.Special.tests_basic_info.column_names()

                if "id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_id(action_params['id'])
                elif "user_id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_user_id(action_params['user_id'])
                elif "method_id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_method_id(action_params['method_id'])
                elif "test_type" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_test_type(action_params['test_type'])
                elif "launch_date" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_launch_date(action_params['launch_date'])
                elif "test_id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Test.by_test_id(action_params['test_id'])
                else:
                    data = "No option was recognized. No action was performed.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableTest.table_name,
                                                           where, 0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableTest.column_names, path,
                                                    True)
                else:
                    return vysledek
            elif action_params['task_subtype'] == "subtest":

                if "id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id(action_params['id'])
                elif "test_id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_test_id(action_params['test_id'])
                elif "dataset_id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(action_params['dataset_id'])
                elif "selection_id" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_selection(
                        action_params['selection_id'])
                else:
                    data = "No option was recognized. No action was performed.\n"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name,
                                                           where, 0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableSubtest.column_names, path,
                                                    True)
                else:
                    return vysledek, sql_database.DatabaseHelper.TableSubtest.column_names
            elif action_params['task_subtype'] == "selection":
                if "dataset_selection" in action_params:
                    if "id_dataset" in action_params:
                        if not file_database.dataset_exist(action_params['id_dataset']):
                            data = "Data set with id {0} does not exist.\n".format(action_params['id_dataset'])
                            if file_control:
                                file_formats.write_query_result([data], "", path, False)
                                return
                            else:
                                return data
                    selections = file_database.get_data_set_selections(action_params['id_dataset'])
                    if file_control:
                        file_formats.write_query_result([selections], [], action_params['output_file'], True)
                        return True
                    else:
                        return True, selections
                if "metadata" in action_params:
                    if "id_dataset" in action_params and "id_selection" in action_params:
                        if not file_database.dataset_exist(action_params['id_dataset']):
                            data = "Data set with id {0} does not exist.\n".format(action_params['id_dataset'])
                            if file_control:
                                file_formats.write_query_result([data], "", path, False)
                                return
                            else:
                                return data
                    meta = file_database.get_selection_metadata(action_params['id_dataset'], action_params['id_selection'])
                    if file_control:
                        file_formats.write_query_result([meta], [], action_params['output_file'], True)
                        return True
                    else:
                        return True, {"meta": meta}
                if "all" in action_params and action_params["all"] == "y":
                    data = {}
                    assays = file_database.get_assays()
                    for assay in assays:
                        sels = file_database.get_data_set_selections(assay)
                        data[assay] = sels
                    if file_control:
                        file_formats.write_run_query_result(data, action_params['output_file'], True)
                    else:
                        return data
                else:
                    if not file_database.dataset_exist(action_params['id_dataset']):
                        data = "Data set with id {0} does not exist.\n".format(action_params['id_dataset'])
                        if file_control:
                            file_formats.write_query_result([data], "", path, False)
                            return
                        else:
                            return data
                    if not file_database.selection_exist(action_params['id_dataset'], action_params['id_selection']):
                        data = "Selection with id {0} from data set id {1} does not exist.\n".format(
                            action_params['id_selection'], action_params['id_dataset'])
                        if file_control:
                            file_formats.write_query_result([data], "", path, False)
                            return
                        else:
                            return data
                    train = file_database.get_selection_train(action_params['id_dataset'], action_params['id_selection'])
                    valid = file_database.get_selection_valid(action_params['id_dataset'], action_params['id_selection'])
                    test = file_database.get_selection_test(action_params['id_dataset'], action_params['id_selection'])
                    meta = file_database.get_selection_metadata(action_params['id_dataset'], action_params['id_selection'])
                    if file_control:
                        file_formats.export_selection(train, valid, test, meta, action_params['output_file'])
                        return True
                    else:
                        return {"train": train, "valid": valid, "test": test, "meta": meta}
            elif action_params['task_subtype'] == "user":

                if 'id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.User.by_id(action_params['id'])
                elif 'username' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.User.by_username(action_params['username'])
                elif 'email' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.User.by_email(action_params['email'])
                else:
                    text = 'Action wasn\'t recognized'
                    if file_control:
                        file_formats.write_query_result([text], "", path, False)
                        return
                    else:
                        return text
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableUs.table_name, where,
                                                           0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableUs.column_names, path, True)
                else:
                    return vysledek
            elif action_params['task_subtype'] == "dataset":

                if "metadata" in action_params:
                    if "id_dataset" in action_params:
                        if not file_database.dataset_exist(action_params['id_dataset']):
                            data = "Data set with id {0} does not exist.\n".format(action_params['id_dataset'])
                            if file_control:
                                file_formats.write_query_result([data], "", path, False)
                                return
                            else:
                                return data
                    meta = file_database.get_metadata(action_params['id_dataset'])
                    if file_control:
                        file_formats.write_query_result([meta], [], action_params['output_file'], True)
                        return True
                    else:
                        return True, {"meta": meta}
                if "selection_number" in action_params:
                    if "id_dataset" in action_params:
                        if not file_database.dataset_exist(action_params['id_dataset']):
                            data = "Data set with id {0} does not exist.\n".format(action_params['id_dataset'])
                            if file_control:
                                file_formats.write_query_result([data], "", path, False)
                                return
                            else:
                                return data
                    number = file_database.get_dataset_selection_number(action_params['id_dataset'])
                    if file_control:
                        file_formats.write_query_result([number], [], action_params['output_file'], True)
                        return True
                    else:
                        return True, number
                if "all" in action_params and action_params["all"] == "y":
                    assays = file_database.get_assays()
                    if file_control:
                        file_formats.write_run_query_result(assays, action_params['output_file'], True)
                    else:
                        return assays
                else:
                    if not file_database.dataset_exist(action_params['id_dataset']):
                        data = "Data set with id {0} does not exist.\n".format(action_params['id_dataset'])
                        if file_control:
                            file_formats.write_query_result([data], "", path, False)
                            return
                        else:
                            return data
                    if "sdf_file" in action_params:
                        sdf_path = file_database.get_path_sdf(action_params['id_dataset'])
                        with open(sdf_path) as stream:
                            s = stream.read()
                        with open(action_params['output_file'], "w") as output:
                            output.write(s)
                        return True
                    else:
                        meta = file_database.get_metadata(action_params['id_dataset'])
                        active = file_database.get_active(action_params['id_dataset'])
                        inactive = file_database.get_inactive(action_params['id_dataset'])
                        if file_control:
                            file_formats.export_dataset(active, inactive, meta, action_params['output_file'])
                            return True
                        else:
                            return True, {"meta": meta, "active": active, "inactive": inactive}
            elif action_params['task_subtype'] == "file":

                if 'id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.File.by_id(action_params['id'])
                elif 'subtest_id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.File.by_subtest_id(action_params['subtest_id'])
                else:
                    text = 'Action wasn\'t recognized'
                    if file_control:
                        file_formats.write_query_result([text], "", path, False)
                        return
                    else:
                        return text
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableFils.table_name, where,
                                                           0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableFils.column_names, path,
                                                    True)
                else:
                    return vysledek
            elif action_params['task_subtype'] == "method":

                if "copy" in action_params:
                    if "method_id" not in action_params:
                        text = 'Method_id key wasn\'t found'
                        if file_control:
                            file_formats.write_query_result([text], "", path, False)
                            return
                        else:
                            return text
                    path = file_database.get_method_path_by_id(action_params['method_id'])
                    if file_control:
                        file_formats.write_query_result([path], "", path, False)
                        return
                    else:
                        return path
                if "choose_methods" in action_params:
                    if "data_set_id" in action_params:
                        where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(
                            action_params["data_set_id"])
                        if "selection_id" in action_params:
                            where += " and " + sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_selection(
                                action_params["selection_id"])
                        result = sql_database.read_from_database(
                            sql_database.DatabaseHelper.Clauses.Special.method_subtest_id.what(),
                            sql_database.DatabaseHelper.TableMethod.table_name,
                            where, 0,
                            sql_database.DatabaseHelper.Clauses.Special.method_subtest_id.join())
                        if file_control:
                            file_formats.write_query_result(result,
                                                            sql_database.DatabaseHelper.Clauses.Special.method_subtest_id.column_names(),
                                                            path, True)
                        else:
                            return result, sql_database.DatabaseHelper.Clauses.Special.method_subtest_id.column_names()
                    else:
                        text = 'Data_set_id key key wasn\'t found'
                        if file_control:
                            file_formats.write_query_result([text], "", path, False)
                            return
                        else:
                            return text
                if "methods_analysis" in action_params:
                    if "method_id" in action_params:
                        where = sql_database.DatabaseHelper.Clauses.Where.Method.by_id(action_params['method_id'])
                        result = sql_database.read_from_database(
                            sql_database.DatabaseHelper.Clauses.Special.method_data.what(),
                            sql_database.DatabaseHelper.TableMethod.table_name,
                            where, 0,
                            sql_database.DatabaseHelper.Clauses.Special.method_data.join())
                        if file_control:
                            file_formats.write_query_result(result,
                                                            sql_database.DatabaseHelper.Clauses.Special.method_data.column_names(),
                                                            path, True)
                        else:
                            return result, sql_database.DatabaseHelper.Clauses.Special.method_data.column_names()
                    if "method_data" in action_params:
                        if "data_set_id" in action_params and "selection_id" in action_params:
                            where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(
                                action_params["data_set_id"])
                            where += " and " + sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_selection(
                                action_params["selection_id"])
                            result = sql_database.read_from_database(
                                sql_database.DatabaseHelper.Clauses.Special.method_data.what(),
                                sql_database.DatabaseHelper.TableMethod.table_name,
                                where, 0,
                                sql_database.DatabaseHelper.Clauses.Special.method_data.join())
                            if file_control:
                                file_formats.write_query_result(result,
                                                                sql_database.DatabaseHelper.Clauses.Special.method_data.column_names(),
                                                                path, True)
                            else:
                                return result, sql_database.DatabaseHelper.Clauses.Special.method_data.column_names()
                        else:
                            text = 'Data_set_id key or selection_id key wasn\'t found'
                            if file_control:
                                file_formats.write_query_result([text], "", path, False)
                                return
                            else:
                                return text
                    if "data_set_id" in action_params and "selection_id" in action_params:
                        where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(
                            action_params["data_set_id"])
                        where += " and " + sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_selection(
                            action_params["selection_id"])
                        result = sql_database.read_from_database(
                            sql_database.DatabaseHelper.Clauses.Special.method_subtest_id_analysis.what(),
                            sql_database.DatabaseHelper.TableMethod.table_name,
                            where, 0,
                            sql_database.DatabaseHelper.Clauses.Special.method_subtest_id_analysis.join())
                        if file_control:
                            file_formats.write_query_result(result,
                                                            sql_database.DatabaseHelper.Clauses.Special.method_subtest_id_analysis.column_names(),
                                                            path, True)
                        else:
                            return result, sql_database.DatabaseHelper.Clauses.Special.method_subtest_id_analysis.column_names()
                    else:
                        text = 'Data_set_id key or selection_id key wasn\'t found'
                        if file_control:
                            file_formats.write_query_result([text], "", path, False)
                            return
                        else:
                            return text
                if 'id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Method.by_id(action_params['id'])
                elif 'method_type' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Method.by_method_type(
                        action_params['method_type'])
                elif 'l_name' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Method.by_l_name(action_params['l_name'])
                elif "s_name" in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Method.by_s_name(action_params['s_name'])
                elif "all" in action_params and action_params["all"] == "y":
                    where = None
                else:
                    text = 'Action wasn\'t recognized'
                    if file_control:
                        file_formats.write_query_result([text], "", path, False)
                        return
                    else:
                        return text
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name,
                                                           where, 0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableMethod.column_names, path,
                                                    True)
                else:
                    return vysledek
            elif action_params['task_subtype'] == "analyze":

                if 'id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Analyze.by_id(action_params['id'])
                elif 'subtest_id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Analyze.by_subtest_id(action_params['subtest_id'])
                else:
                    text = 'Action wasn\'t recognized'
                    if file_control:
                        file_formats.write_query_result([text], "", path, False)
                        return
                    else:
                        return text
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableAnalyze.table_name,
                                                           where,
                                                           0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableAnalyze.column_names, path,
                                                    True)
                else:
                    return vysledek, sql_database.DatabaseHelper.TableAnalyze.column_names
            elif action_params['task_subtype'] == "molecule":

                if "molecule_info" in action_params:
                    if 'molecule_name' in action_params and "dataset_id" in action_params:
                        where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_molecule_name(
                            action_params['molecule_name'])
                        where = where + " and " + sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id_dataset(
                            action_params['dataset_id'])
                        vysledek = sql_database.read_from_database(
                            sql_database.DatabaseHelper.Clauses.Special.molecule_info.what(),
                            sql_database.DatabaseHelper.TableMol.table_name,
                            where,
                            0, sql_database.DatabaseHelper.Clauses.Special.molecule_info.join())
                        if file_control:
                            file_formats.write_query_result(vysledek,
                                                            sql_database.DatabaseHelper.Clauses.Special.molecule_info.column_names(),
                                                            path, True)
                        else:
                            return vysledek, sql_database.DatabaseHelper.Clauses.Special.molecule_info.column_names()
                    else:
                        data = "Molecule_name key wasn't found"
                        if file_control:
                            file_formats.write_query_result([data], [], path, False)
                            return
                        else:
                            return data

                if "true_value" in action_params:
                    if 'molecule_name' in action_params and "data_set_id" in action_params:
                        actives = file_database.get_active(action_params["data_set_id"])
                        if action_params["molecule_name"] in actives:
                            result = "1"
                        else:
                            result = "0"
                        if file_control:
                            file_formats.write_query_result([result], [],
                                                            path, True)
                        else:
                            return result
                    else:
                        data = "Molecule_name key or data-set ID key wasn't found"
                        if file_control:
                            file_formats.write_query_result([data], [], path, False)
                            return
                        else:
                            return data
                if "image" in action_params:
                    if "molecule_name" not in action_params or "dataset_id" not in action_params:
                        data = "Molecule wasn't recognized"
                        if file_control:
                            file_formats.write_query_result([data], [], path, False)
                            return
                        else:
                            return data
                    if "png_file" not in action_params:
                        data = "PNG file wasn't given"
                        if file_control:
                            file_formats.write_query_result([data], [], path, False)
                            return
                        else:
                            return data
                    # getting molecule
                    try:
                        mol_object = file_database.get_molecule_object(action_params["dataset_id"],
                                                                       action_params['molecule_name'])
                        draw.MolToFile(mol_object, action_params['png_file'])
                        r_value = True
                    except Exception as e:
                        print("Warning in creating image" + str(e))
                        r_value = False
                    if file_control:
                        file_formats.write_query_result([[]], [], path,
                                                        r_value)
                    else:
                        return r_value

                if 'id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_id(action_params['subtest_id'])
                elif 'molecule_name' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_molecule_name(
                        action_params['molecule_name'])
                elif 'subtest_id' in action_params:
                    where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_subtest_id(
                        action_params['subtest_id'])
                else:
                    data = "Command wasn't recognized"
                    if file_control:
                        file_formats.write_query_result([data], "", path, False)
                        return
                    else:
                        return data
                vysledek = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMol.table_name, where,
                                                           0)
                if file_control:
                    file_formats.write_query_result(vysledek, sql_database.DatabaseHelper.TableMol.column_names, path, True)
                else:
                    return vysledek, sql_database.DatabaseHelper.TableMol.column_names
            else:
                data = "Command wasn't recognized.\n"
                if file_control:
                    file_formats.write_query_result([data], "", path, False)
                    return
                else:
                    return data
        elif action_params['task_type'] == 'initialize':
            if "dataset_database" in action_params and action_params['dataset_database'] == "True":
                update_need, local_date, net_date, net_version = file_database.update_dataset_database()
                if update_need:
                    file_database.updating_db(local_date, net_date, net_version)
            if "SQL_database" in action_params and action_params['SQL_database'] == "True":
                if "delete_table_tests" in action_params and action_params['delete_table_results'] == "True":
                    t_tests = True
                    delete_all_tests()
                else:
                    t_tests = False
                if "delete_table_subtests" in action_params and action_params['delete_table_results'] == "True":
                    t_subtests = True
                else:
                    t_subtests = False
                if "delete_table_analyzes" in action_params and action_params['delete_table_results'] == "True":
                    t_analyzes = True
                else:
                    t_analyzes = False
                if "delete_table_users" in action_params and action_params['delete_table_users'] == "True":
                    t_users = True
                else:
                    t_users = False
                if "delete_table_files" in action_params and action_params['delete_table_files'] == "True":
                    t_files = True
                else:
                    t_files = False
                if "delete_table_molecules" in action_params and action_params['delete_table_molecules'] == "True":
                    t_molecules = True
                else:
                    t_molecules = False
                if "delete_table_methods" in action_params and action_params['delete_table_results'] == "True":
                    t_methods = True
                    delete_all_methods()
                else:
                    t_methods = False
                sql_database.init_sqldatabase(t_users, t_tests, t_molecules, t_analyzes, t_files, t_subtests, t_methods)
        elif action_params['task_type'] == 'front_end':
            if action_params['task_subtype'] == "check_user":
                if "username" not in action_params:
                    if file_control:
                        file_formats.write_query_result([()], [], path, False)
                    else:
                        return ()
                if action_params['username'] == "host":
                    id = 1
                else:
                    id = verify_user(action_params['username'])
                    if int(id) == -1:
                        data = "Bad username"
                        if file_control:
                            file_formats.write_query_result([data], [], path, False)
                        else:
                            return data
                if file_control:
                    file_formats.write_query_result([str(id)], [], path, True)
                else:
                    return id
            elif action_params['task_subtype'] == "transform_path":
                if "path" in action_params:
                    return os.path.abspath(action_params["path"])
                else:
                    return ""
            elif action_params['task_subtype'] == "calculate":
                if not "subtest_id" in action_params or not "molecules_number" in action_params:
                    return {'EF 0.005': 0, 'EF 0.01': 0, 'EF 0.02': 0, 'EF 0.05': 0, 'AUC': 0, 'scores': []}
                else:
                    # get molecules of subtest ID
                    where = sql_database.DatabaseHelper.Clauses.Where.Subtest.by_id(action_params['subtest_id'])
                    subtest = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name,
                                                              where, 0)
                    if len(subtest) == 0:
                        return {'EF 0.005': 0, 'EF 0.01': 0, 'EF 0.02': 0, 'EF 0.05': 0, 'AUC': 0, 'scores': []}
                    where = sql_database.DatabaseHelper.Clauses.Where.Molecule.by_subtest_id(
                        action_params['subtest_id'])
                    molecules = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMol.table_name, where,
                                                                0)
                    sorting = []
                    for molecule in molecules:
                        sorting.append({
                            "name": molecule[2],
                            "similarity": molecule[3]
                        })
                    sorting = sorted(sorting, key=lambda m: m['similarity'], reverse=True)
                    active = file_database.get_active(subtest[0][2])
                    inactive = file_database.get_inactive(subtest[0][2])
                    if int(action_params["molecules_number"]) == -1 or int(action_params["molecules_number"]) > len(
                            sorting):
                        ans = analysis.analyze(sorting, active, inactive)
                    else:
                        ans = analysis.analyze(sorting[:int(action_params["molecules_number"])], active, inactive)
                    # SQL column readable names: "AUC", "EF 0.005", "EF 0.01", "EF 0.02", "EF 0.05"
                    # analysis keys: 'EF0.005', 'EF0.01', 'EF0.02', 'EF0.05', 'AUC'
                    return {'EF 0.005': ans['EF0.005'], 'EF 0.01': ans['EF0.01'], 'EF 0.02': ans['EF0.02'],
                            'EF 0.05': ans['EF0.05'], 'AUC': ans['AUC']}
    except Exception as e:
        print("Warning: " + str(e))
        if file_control:
            file_formats.write_query_result([str(e)], [], path,
                                            False)
        else:
            return [str(e)]


def write_info_about_test(test_id, result_path, file_control):
    result_dictionary = {}
    if file_control:
        if not os.path.exists(os.path.dirname(result_path)):
            os.makedirs(os.path.dirname(result_path))
    scs = True
    if not isinstance(test_id, int):
        if file_control:
            file_formats.write_query_result(['Bad request, test ID isn\'t a number: ' + str(test_id)], [], result_path, False)
        else:
            return 'Bad request, test ID isn\'t exist a number: ' + str(test_id)
    test_info = None
    try:
        test_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableTest.table_name,
                                                    sql_database.DatabaseHelper.Clauses.Where.Test.by_id(
                                                        test_id), 0)
    except Exception as e:
        if file_control:
            file_formats.write_query_result(["Exception: " + str(e), "Exception 1: " + str(test_id)], [], result_path,
                                            False)
            return
        else:
            return "Exception: " + str(e) + ", Exception 1: " + str(test_id)
    if test_info == None:
        if file_control:
            file_formats.write_query_result(['Result of the test is None'], [], result_path, False)
            return
        else:
            return 'Result of the test is None'
    try:
        test_info = test_info[0]
    except:
        if file_control:
            file_formats.write_query_result(['Bad request, test ID doesn\'t exist'], [], result_path, False)
        else:
            return 'Bad request, test ID doesn\'t exist'
    try:
        user_id = test_info[1]
        result_dictionary['test_id'] = ["ID of test", test_id]
        if test_info[2] == sql_database.DatabaseHelper.Test_types.local_series:
            type = "Local computer series"
        elif test_info[2] == sql_database.DatabaseHelper.Test_types.local_paralel:
            type = "Local computer parallel"
        elif test_info[2] == sql_database.DatabaseHelper.Test_types.metacentrum:
            type = "Metacentrum"
        elif test_info[2] == sql_database.DatabaseHelper.Test_types.remove:
            type = "Remote computers"
        else:
            type = "Unknown"
        result_dictionary['test_type'] = ["Type of test", type]
        result_dictionary['subcount'] = ["Count of subtests", test_info[3]]
        method_id = test_info[4]
        try:
            result_dictionary['date'] = ["Launch date", (test_info[5]).isoformat()]
        except AttributeError as ae:
            date_parts = str(test_info[5]).split("-")
            result_dictionary['date'] = ["Launch date", date_parts[2] + str("/") + date_parts[1] + str("/") + date_parts[0]]
        result_dictionary[
            'method_type'] = ["Type of method",
                              sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMethod.table_name,
                                                              sql_database.DatabaseHelper.Clauses.Where.Method.by_id(
                                                                  method_id), 0)[0][1]]
        result_dictionary['test_description'] = ['Description', test_info[7]]
        test_name = test_info[0]
        subtests_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableSubtest.table_name,
                                                        sql_database.DatabaseHelper.Clauses.Where.Subtest.by_test_id(
                                                            test_name), 0)
        i = 1
        for subtest in subtests_info:
            sub_dict = {
                "molecule_column_names": ["Name", "Score", "Activity"],
                "subtest_id": ["ID of subtest", subtest[0]],
                "id_dataset": ["ID of data-set", subtest[2]],
                "id_selection": ["ID of selection", subtest[3]]
            }
            analyze_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableAnalyze.table_name,
                                                           sql_database.DatabaseHelper.Clauses.Where.Analyze.by_subtest_id(
                                                               subtest[0]), 0)
            try:
                analyze_info = analyze_info[0]
            except:
                sub_dict["analyze_error"] = "True"
                continue
            sub_dict["auc"] = ["AUC", analyze_info[2]]
            sub_dict["ef_0_005"] = ["EF 0.005", analyze_info[3]]
            sub_dict["ef_0_01"] = ["EF 0.01", analyze_info[4]]
            sub_dict["ef_0_02"] = ["EF 0.02", analyze_info[5]]
            sub_dict["ef_0_05"] = ["EF 0.05", analyze_info[6]]
            molecules_info = sql_database.read_from_database("*", sql_database.DatabaseHelper.TableMol.table_name,
                                                             sql_database.DatabaseHelper.Clauses.Where.Molecule.by_subtest_id(
                                                                 subtest[0]), 0)
            active = file_database.get_active(subtest[2])
            molecules_values = []
            for mol in molecules_info:
                if mol[2] in active:
                    type = "Active"
                else:
                    type = "Inactive"
                molecules_values.append([mol[2], mol[3], type])
            sub_dict['molecule_values'] = molecules_values
            key_name = 'subtest_' + str(i)
            result_dictionary[key_name] = sub_dict
            i += 1
        scs = True
    except Exception as e:
        result_dictionary['error'] = ["Error ", "Error occurred in writting result: " + str(e)]
        result_dictionary["error_1"] = ["Error ", "Error in: " + str(test_id)]
        scs = False
    if file_control:
        file_formats.write_run_query_result(result_dictionary, result_path, scs)
    else:
        return result_dictionary

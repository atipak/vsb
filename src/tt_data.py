import random


def sample_train_data(act, inact, number_act='5', number_in='5', technique='classic'):
    """
    This method takes active and inactive molecules and makes train data set from them.
    :param act: Active molecules in data set
    :param inact: Inactive molecules in data set
    :param number_act: Count of active molecules in resulting set.
    :param number_in: Count of inactive molecules in resulting set.
    :param technique: Technique of selection.
    :return: List with two list elements which are active train molecules and inactive train molecules.
    """
    train_data = []
    active = []
    inactive = []
    if int(number_act) > len(act):
        number_act = len(act)
    if int(number_in) > len(inact):
        number_in = len(inact)
    # options of selection
    if technique == 'classic':
        for x in range(int(number_act)):
            active.append(act[x])
        for x in range(int(number_in)):
            inactive.append(inact[x])
        train_data.append(active)
        train_data.append(inactive)
    elif technique == 'random':
        num = int(number_act)
        # active
        while num > 0:
            x = random.randint(0, len(act) - 1)
            if act[x] not in active:
                active.append(act[x])
                num -= 1
        # inactive
        num = int(number_in)
        while num > 0:
            x = random.randint(0, len(inact) - 1)
            if inact[x] not in inactive:
                inactive.append(inact[x])
                num -= 1
        train_data.append(active)
        train_data.append(inactive)
    return train_data


def sample_test_data(active, inactive, number_act, number_inactive, technique='classic', chosen_act=[]):
    """
    This method takes active and inactive molecules and makes train data set from them.
    :param active: Active molecules in data set
    :param inactive: Inactive molecules in data set
    :param number_act: Count of active molecules in resulting set.
    :param number_inactive: Count of inactive molecules in resulting set.
    :param technique: Technique of selection.
    :param chosen_act: List with forbidden molecules. First element active molecules, second one inactive molecules.
    :return: List filled with molecules for testing methods.
    """
    test_data = []
    n_ins_all = 0
    n_ins_act = 0
    if int(number_act) > len(active):
        number_act = len(active)
    if len(inactive) < int(number_inactive):
        number_inactive = len(inactive)
    # takes first X molecules from lists
    if technique == 'classic':
        # active first
        for x in range(number_act):
            test_data.append(active[x])
            n_ins_act += 1
        # other
        for x in range(number_inactive):
            test_data.append(inactive[x])
            n_ins_all += 1
    # chooses X molecules randomly
    elif technique == 'random':
        num = int(number_act)
        # active
        while num > 0:
            x = random.randint(0, len(active) - 1)
            if active[x] not in test_data:
                test_data.append(active[x])
                num -= 1
        num = int(number_inactive)
        # inactive
        while num > 0:
            x = random.randint(0, len(inactive) - 1)
            if inactive[x] not in test_data:
                test_data.append(inactive[x])
                num -= 1
    # takes complement to forbidden list
    elif technique == 'complement':
        chosen_active = chosen_act[0]
        chosen_inactive = chosen_act[1]
        for item in active:
            if item not in chosen_active:
                test_data.append(item)
        for item in inactive:
            if item not in chosen_inactive:
                test_data.append(item)
    random.shuffle(test_data)
    return test_data


def mk_simple_selection(active, inactive):
    """
    Create a simple selection from given active and inactive molecules. Count in train set is chosen randomly, test set
    is made from complement of train molecules on data set.
    :param active: Active molecules
    :param inactive: Inactive molecules
    :return: List with three elements. First element is a dictionary with metadata, second one is a list with train data
    and last element is a list with test data.
    """
    answer = []
    rep = 20
    x = rep
    best_train = []
    best_test = []
    maxnum_act_tr = 0
    maxnum_inact_tr = 0
    maxnum_all = 0
    maxnum_act_te = 0
    maxall = len(active) + len(inactive)
    while x > 0:
        num_act_tr = random.randint(5, round((len(active) - 1) * 0.50))
        num_inact_tr = random.randint(5, round((len(active) - 1) * 0.50))
        train = sample_train_data(active, inactive, num_act_tr, num_inact_tr, 'random')
        test = sample_test_data(active, inactive, 0, 0, 'complement', train)
        best_test = test
        best_train = train
        maxnum_act_tr = num_act_tr
        maxnum_inact_tr = num_inact_tr
    x -= 1
    print("Pocet prvku v datasetu: ", maxall)
    print("Max aktivnich v train: ", maxnum_act_tr)
    print("Max neaktivnich v train: ", maxnum_inact_tr)
    print("Max neaktivnich v test: ", maxnum_all)
    print("Max aktivnich v test: ", maxnum_act_te)
    metadata = {"Number of moleculs in dataset: ": maxall, "Active in train: ": maxnum_act_tr,
                "Inactive in train: ": maxnum_inact_tr, "Inactive in test: ": maxnum_all, "Active in test: ": maxnum_act_te}
    answer.append(metadata)
    answer.append(best_train)
    answer.append(best_test)
    return answer


def user_answer(selection):
    return True

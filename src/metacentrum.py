import pysftp
from general import *
import remote_launching

start_time = 0


def metacentrum_services(config, metadata):
    """
    Basic function for creating test data for a launch test on Metacentrum.

    :param config: Test configuration.
    :param metadata: Metatadata of test.
    :return: Nothing, SQL ID of test
    """
    # zakladni nastaveni
    config, select, recid_test = start_and_check_test(config, sql_database.DatabaseHelper.Test_types.metacentrum)
    # nalezeni front end uzlu
    if "front_ends_nodes" not in config:
        print("'front_ends_nodes' key doesnt exist")
        return False, recid_test
    front_end_nodes = config["front_ends_nodes"]
    if not os.path.exists(front_end_nodes):
        print("File with front ends nodes doesnt exist")
        return False, recid_test
    node = choose_front_end_node(front_end_nodes)
    # precteni prihlasovacich udaju
    if "login_data" not in config:
        print("'login_data' key doesnt exist ")
        return False, recid_test
    login_data = config['login_data']
    if not os.path.exists(login_data):
        print("File with login data doesnt exist")
        return False, recid_test
    l_data = get_login_data(login_data)
    # cycle for executing tests
    repeat = int(config['repeat'])
    # end condition - bad test - max selection which can be used
    max_number = get_max_repeat(config)
    successful = 0
    selected = {"new_selected": select}
    while actual_done(config, len(select), successful) <= repeat:
        successful += metacentrum_service(config, selected['new_selected'], config['test_id'], config['test_path'],
                                          config['first_result'] + successful, node, l_data, recid_test)
        selected = choose_selections_after(repeat - successful, select)
        select = selected['all']
        if successful == repeat:
            break
        if len(select) > max_number:
            print("Too many selections used")
            return False, recid_test
    return True, recid_test


def metacentrum_service(config, select, safe_test_number, test_path, first_result, front_node, login_data, recid_test):
    """
    Function for Metacentrum service.

    :param config: Test data
    :param select: Selected selections
    :param safe_test_number: ID of test.
    :param test_path: Path to a folder associated with test data.
    :param first_result: ???
    :param front_node: The chose front node on Metacentrum.
    :param login_data: A path to file with login data on Metacentrum.
    :param recid_test: SQL ID of test
    :return: Count of successful tests.
    """
    cnopts = pysftp.CnOpts(knownhosts='')
    cnopts.hostkeys = None
    successful = 0
    check_temp_folder()
    OS = get_os_class('LINUX')
    remote_path_separator = OS.path_separator

    # vytvoreni shell-skriptu a dat
    ####
    method_id = remote_launching.get_method_id(config)
    skripts_list = []
    for s in select:
        dataset = s['dataset']
        selection = s['selection']
        d_remote = remote_launching.config_for_remote(config, method_id, dataset, selection, remote_path_separator, True)
        input_files = [d_remote['method_folder']]
        output_files = [d_remote['relative_method_folder']]
        d_local = create_paths_for_local_files(dataset, selection, method_id, config, recid_test)
        prepare_data_for_transport(d_local, d_remote)
        remote_launching.make_file_of_configuration(d_remote, d_local['f_config_file_path'])
        write_shell_script(config, d_local['f_script_path'], front_node, input_files, output_files,
                           d_remote, d_remote["base_folder_path"])
        skripts_list.append(os.path.basename(d_local['f_script_path']))
    ####

    # preneseni souboru na front-end
    job_ids = {}
    try:
        with pysftp.Connection(front_node['name'], username=login_data['login'], password=login_data['password'],
                               cnopts=cnopts) as sftp:
            d_remote = remote_launching.config_for_remote(config, method_id, 1, 1, remote_path_separator, True)
            d_r = create_paths_for_remote_files(d_remote['method_folder'], remote_path_separator)
            (head, tail) = os.path.split(d_r['zip_file_tr_path'])
            sftp.makedirs(head)
            d_local = create_paths_for_local_files(1, 1, method_id, config, recid_test)
            transfer_data_to_front_node(sftp, d_local, d_r)
            # unzipping files
            command_for_unzip = 'unzip -ou ' + d_r['zip_file_tr_path'] + " -d " + head
            answer = sftp.execute(command_for_unzip)
            # delete zip file
            command_for_delete_zip_file = 'rm ' + d_r['zip_file_tr_path']
            answer = sftp.execute(command_for_delete_zip_file)
            # spusteni shellu
            # moving into directory method_id
            start_time = time.time()
            for skript in skripts_list:
                part = str(skript).split('.')[0]
                index = part.find('_') + 1
                key_original = part[index:]
                last_index = key_original.rfind("_")
                dataset = key_original[:last_index]
                selection = key_original[last_index + 1:]
                key = dataset + "#" + selection
                test_folder_path = d_remote['method_folder'] + remote_path_separator + "test_" + key_original
                command_for_scripts = create_qsub_query("../" + os.path.basename(d_r['scripts_folder']), skript)
                job_ids[key] = sftp.execute("cd {0};".format(test_folder_path) + command_for_scripts)
    except Exception as e:
        print(e)
        return 0
    # zajisteni vysledku
    command_for_checking_result = 'qstat '
    temp_dictionary = {}
    waiting = True
    while waiting:
        try:
            with pysftp.Connection(front_node['name'], username=login_data['login'], password=login_data['password'],
                                   cnopts=cnopts) as sftp:
                # key = dataset_selection
                # item = job_id
                for key in job_ids.keys():
                    job_id = job_ids[key]
                    decoded_job_id = job_id[0].decode('utf-8')
                    command_for_checking = command_for_checking_result + decoded_job_id
                    answer = sftp.execute(command_for_checking)
                    if is_done(answer):
                        dataset, selection = remote_launching.get_dataset_selection_from_key(key)
                        d_remote = remote_launching.config_for_remote(config, method_id, dataset, selection,
                                                                      remote_path_separator, True)
                        d_local = create_paths_for_local_files(dataset, selection, method_id, config, recid_test)
                        command_success = "cat " + d_remote['success_tr_path']
                        print("Checking test {0}, data-set_selection {1}".format(decoded_job_id, key))
                        if m_write_result(sftp, d_remote, command_success, safe_test_number, successful + 1,
                                          test_path, d_local['path_for_files'], config, recid_test,
                                          int(time.time() - start_time)):
                            successful += 1
                        get_other_files(sftp, d_remote, test_path, decoded_job_id, key)
                        sftp.execute('qdel ' + decoded_job_id)
                        remote_launching.reset_sleep_time()
                    else:
                        temp_dictionary[key] = job_id
                        print("{0} is still running".format(job_id))
        except Exception as e:
            print(e)
        if len(temp_dictionary) == 0:
            waiting = False
        job_ids = temp_dictionary
        temp_dictionary = {}
        print("The main thread is going to sleep")
        time.sleep(remote_launching.get_sleep_time())

    # uklid pocitacu
    try:
        d_local = create_paths_for_local_files(1, 1, method_id, config, recid_test)
        remote_launching.clean_local_temp(d_local["local_temp_path"])
        d_remote = remote_launching.config_for_remote(config, method_id, 1, 1, remote_path_separator, True)
        if "clean_remote" in config and config['clean_remote'] == 'True':
            with pysftp.Connection(front_node['name'], username=login_data['login'], password=login_data['password'],
                                   cnopts=cnopts) as sftp:
                # remove scripts
                # remove sdf
                # remove test folders
                print("Remote computer cleaning")
                d_r = create_paths_for_remote_files(d_remote['method_folder'], remote_path_separator)
                for skript in skripts_list:
                    part = str(skript).split('.')[0]
                    index = part.find('_') + 1
                    key_original = part[index:]
                    last_index = key_original.rfind("_")
                    dataset = key_original[:last_index]
                    test_folder_path = d_remote['method_folder'] + remote_path_separator + "test_" + key_original
                    script_path = d_r['scripts_folder'] + remote_path_separator + skript
                    sftp.execute(OS.ret_remove_rec_com(test_folder_path))
                    sftp.execute(OS.ret_remove_rec_com(script_path))
                    sdf = d_remote['method_folder'] + remote_path_separator + "sdf_folder" + remote_path_separator + dataset
                    if sftp.exists(sdf):
                        sftp.execute(OS.ret_remove_rec_com(sdf))
    except Exception as e:
        print(e)
    return successful


def is_done(answer):
    """
    Check if task on Metacentrum is done.

    :param answer: Answer which was got from Metacentrum.
    :return: True/False
    """
    if not str(answer).find("has finished") == -1:
        return True
    else:
        return False


def create_paths_for_local_files(dataset, selection, method_id, config, recid_test):
    """
    Creates paths where local files should be stored.

    :param dataset: ID of data set
    :param selection: ID of selection
    :param method_id: ID of method
    :param config: Configuration
    :return: Dictionary with paths.
    """
    check_temp_folder()
    path_temp_vsb = "metacentrum" + os.sep + str(recid_test) + os.sep + method_id
    local_temp_path = get_temp_folder_path() + os.sep + path_temp_vsb
    if not os.path.exists(local_temp_path):
        os.makedirs(local_temp_path)
    extension_vsb_d_s = remote_launching.test_prefix + str(dataset) + "_" + str(selection)
    extension_vsb_scripts = "folder_scripts"
    extension_vsb_sdf = "sdf_folder"
    path_for_files = local_temp_path + os.sep + extension_vsb_d_s
    if not os.path.exists(path_for_files):
        os.makedirs(path_for_files)
    path_for_scripts = local_temp_path + os.sep + extension_vsb_scripts
    path_for_sdf = os.path.join(local_temp_path, extension_vsb_sdf, str(dataset))
    if not os.path.exists(path_for_scripts):
        os.makedirs(path_for_scripts)
    dictionary = {}
    dictionary['path_for_files'] = path_for_files
    dictionary['local_temp_path'] = local_temp_path
    dictionary['path_for_scripts'] = path_for_scripts
    dictionary['f_dataset_path'] = path_for_files + os.sep + 'selection.json'
    dictionary['f_input_data_path'] = path_for_files + os.sep + 'input.json'
    dictionary['sdf_path'] = file_database.get_path_sdf(dataset)
    dictionary['f_sdf_path'] = path_for_sdf + os.sep + "mols.sdf"
    dictionary['f_config_file_path'] = path_for_files + os.sep + "configuration.json"
    dictionary['skeleton_path'] = "skeleton.py"
    dictionary['f_skeleton_path'] = path_for_files + os.sep + 'skeleton.py'
    dictionary['remote_station_path'] = 'remote_station.py'
    dictionary['f_remote_station_path'] = path_for_files + os.sep + 'remote_station.py'
    dictionary['f_script_path'] = path_for_scripts + os.sep + 'run_' + str(dataset) + '_' + str(selection) + '.sh'
    dictionary['f_zip_file'] = local_temp_path + os.sep + "data.zip"
    if config['typefile'] == 'python':
        dictionary['sim_method_path'] = config['sim_method_path']
        dictionary['f_sim_method_path'] = path_for_files + os.sep + os.path.basename(dictionary['sim_method_path'])
        dictionary['repr_method_path'] = config['repr_method_path']
        dictionary['f_repr_method_path'] = path_for_files + os.sep + os.path.basename(dictionary['repr_method_path'])
    else:
        dictionary['file_method_path'] = config['file_method_path']
        config['file'] = dictionary['file_method_path']
        dictionary['file'] = config['file']
        dictionary['f_file'] = path_for_files + os.sep + os.path.basename(config['file'])
    return dictionary


def create_paths_for_remote_files(remote_path, remote_path_separator):
    """
    Creates paths where should be stored files on remote computer.

    :param remote_path: A basic remote path
    :param remote_path_separator: Separator used on a remote computer.
    :return: Directory with paths.
    """
    extension_vsb_scripts = "folder_scripts"
    directory = {}
    directory['scripts_folder'] = remote_path + remote_path_separator + extension_vsb_scripts
    directory['zip_file_tr_path'] = remote_path + remote_path_separator + 'data_for_transport.zip'
    return directory


def get_login_data(path_to_file):
    """
    Returns login data stored in file.
    :param path_to_file: A path to file with data.
    :return: Login data
    """
    with open(path_to_file, "r") as file:
        json_file = json.load(file)
        return json_file['metacentrum']


def prepare_data_for_transport(d_local, d_remote):
    """
    Creates data files for transport. Type and count of files is chosen by type of test (python, general)
    :param d_local: Dictionary with local paths
    :param d_remote: Dictionary with remote paths
    :return: Nothing
    """
    import shutil
    print("Creating of basic files for the Metacentrum")
    train = file_database.get_selection_train(d_remote['dataset'], d_remote['selection'])
    valid = file_database.get_selection_valid(d_remote['dataset'], d_remote['selection'])
    test = file_database.get_selection_test(d_remote['dataset'], d_remote['selection'])
    metadata = []
    if "file" in d_local:
        file_formats.to_json(train, valid, test, metadata, d_local['f_input_data_path'], d_remote['sdf'])
        if os.path.exists(d_local['file']):
            safe_copytree(d_local['file'], d_local['f_file'])
    else:
        remote_launching.write_selection_file_for_remote(metadata, train, valid, test, d_local['f_dataset_path'])
        if os.path.exists(d_local['sim_method_path']):
            safe_copytree(d_local['sim_method_path'], d_local['f_sim_method_path'])
        if os.path.exists(d_local['repr_method_path']):
            safe_copytree(d_local['repr_method_path'], d_local['f_repr_method_path'])
    if os.path.exists(d_local['sdf_path']):
        if not os.path.exists(os.path.dirname(d_local['f_sdf_path'])):
            os.makedirs(os.path.dirname(d_local['f_sdf_path']))
        shutil.copyfile(d_local['sdf_path'], d_local['f_sdf_path'])
    if os.path.exists(d_local['skeleton_path']):
        shutil.copyfile(d_local['skeleton_path'], d_local['f_skeleton_path'])
    if os.path.exists(d_local['remote_station_path']):
        shutil.copyfile(d_local['remote_station_path'], d_local['f_remote_station_path'])


def transfer_data_to_front_node(sftp, d_local, d_remote):
    """
    Transfers data from local computer to the chosen front node.
    :param sftp: SFTP connection.
    :param d_local: Dictionary with local paths.
    :param d_remote: Dictinary with remote paths.
    :return: Nothing
    """
    print("Creating of a zip file for transfer of files")
    import zipfile
    zip_file = zipfile.ZipFile(d_local['f_zip_file'], mode="w")
    remote_launching.add_folder_to_zip_file(zip_file, d_local['local_temp_path'], True)
    zip_file.close()
    print("The zip file was created")
    print("Transferring of the zip file")
    sftp.put(d_local['f_zip_file'], d_remote['zip_file_tr_path'])
    print("Transferring finished")


def write_shell_script(config, shell_script_path, front_end, input_files, output_files, d_remote, output_data_path):
    """
    Writes a shell script for given task.

    :param config: Configuration.
    :param shell_script_path: A path where the script should be stored.
    :param front_end: Names of front-end nodes
    :param input_files: List with names of input files
    :param output_files: List with names of output files.
    :param command_run: Command which launch the remote station module on remote computer.
    :param output_data_path: A path on front node where output files should be again stored from scratch.
    :return: Nothing
    """
    new_line = "\n"
    datadir = front_end["home"] + "$PBS_O_LOGNAME/"
    with open(shell_script_path, "w", newline='\n') as script:
        # prolog
        script.write("#!/bin/bash")
        script.write(new_line)
        script.write(new_line)
        # rdkit
        # Update environment paths.
        script.write("export RDBASE=/storage/praha1/home/skodape/libs/RDKit_2014_09_1")
        script.write(new_line)
        script.write("export PYTHONPATH=$RDBASE:$PYTHONPATH")
        script.write(new_line)
        script.write("export LD_LIBRARY_PATH=$RDBASE/lib:$LD_LIBRARY_PATH")
        script.write(new_line)
        # Add required modules.
        script.write("module add boost-1.49")
        script.write(new_line)
        script.write("module add numpy-1.7.1-py2.7")
        script.write(new_line)
        script.write("module add scipy-0.12.0-py2.7")
        script.write(new_line)
        script.write("module add sklearn-0.14.1-py2.7")
        script.write(new_line)
        # end rdkit
        script.write(new_line)
        script.write(new_line)
        script.write("trap 'clean_scratch' TERM EXIT")
        script.write(new_line)
        script.write(new_line)
        script.write("DATADIR=\"" + datadir + "\"")
        script.write(new_line)
        script.write(new_line)
        # transferring data on scratch
        for file in input_files:
            script.write("cp -r $DATADIR/" + file + " $SCRATCHDIR  || exit 1")
            script.write(new_line)
        script.write("cd $SCRATCHDIR || exit 2 ")
        script.write(new_line)
        script.write(new_line)
        # loading modules
        # script.write("module add python-3.4.1")
        #  initialization of chem
        if (config["typefile"] == "python" and "optimalization" == "True") or (
                        "chem" in config and config["chem"] == "True"):
            script.write(new_line)
        # moving to test_folder
        script.write("cd $SCRATCHDIR/{0}".format(d_remote['relative_test_folder']))
        script.write(new_line)
        # launching test
        script.write(
            "python $DATADIR/" + d_remote['test_folder'] + '/' + d_remote['remote_station'] + ' -m $SCRATCHDIR/' +
            d_remote[
                'relative_test_folder'] + "/" + remote_launching.configuration_file_name)
        script.write(new_line)
        script.write(new_line)
        # transferring files from scratch
        for file in output_files:
            script.write(
                "cp -r $SCRATCHDIR/" + file + " $DATADIR/{0} || export CLEAN_SCRATCH=true".format(output_data_path))
            script.write(new_line)
        # removing pyc
        script.write("find $DATADIR/" + d_remote['test_folder'] + " -name \"*.pyc\" -exec rm -f {} \;")


def choose_front_end_node(path_to_file):
    """
    Returns a chosen front-end node.

    :param path_to_file: A path where the file with written nodes is stored.
    :return: The chosen node.
    """
    with open(path_to_file, "r") as file:
        json_file = json.load(file)
        length = len(json_file['nodes'])
        import random
        r = random.randint(0, length - 1)
        return json_file['nodes'][0]


def m_write_result(sftp, d_remote, check_command, id_test, id_result, test_path, pc_name, data, recid_test, runtime=0):
    """
    If a result of test on remote computer is OK, then downloads data and insert them into SQL database.
    :param recid_test: SQL ID of test
    :param sftp: SFTP connection.
    :param d_remote: Dictionary with remote paths.
    :param check_command: Command which return if test is OK or not.
    :param id_test: ID of test.
    :param id_result: ID of result.
    :param test_path: Path to folder connected with test.
    :param pc_name: Name of pc or a right path.
    :param data: Some additional data.
    :param runtime: runtime
    :return: True/False by success of result of test.
    """
    print("Checking of a remote computer result.")
    l_result = sftp.execute(check_command)
    state = str(l_result[0])
    if not state.find('True') == -1:
        return remote_launching.success(sftp, d_remote, id_test, id_result, test_path, pc_name, data, recid_test,
                                        runtime)
    else:
        print("Test failed")
        return False


def get_other_files(sftp, d_remote, test_path, id, d_s):
    """
    Gets data from front-node to local computer.
    :param sftp: Binding to front-node.
    :param d_remote: Dictionary with paths.
    :param test_path: Test path.
    :return:
    """
    dataset, selection = remote_launching.get_dataset_selection_from_key(d_s)
    skript_name = 'run_' + dataset + '_' + selection + '.sh'
    # output file
    file_name = skript_name + ".o" + str(id).split(".")[0]
    file_local_path = os.path.join(os.path.abspath(test_path), file_name)
    file_remote_path = d_remote['test_folder'] + "/" + file_name
    sftp.get(localpath=file_local_path, remotepath=file_remote_path)
    # error file
    file_name = skript_name + ".e" + str(id).split(".")[0]
    file_local_path = os.path.join(os.path.abspath(test_path), file_name)
    file_remote_path = d_remote['test_folder'] + "/" + file_name
    sftp.get(localpath=file_local_path, remotepath=file_remote_path)


def create_qsub_query(skript_folder, skript_name):
    """
    Creates a qsub query for launching test on Metacentrum.
    :return: Query as a string.
    """
    select = 1
    ncpus = 2
    mem = 1
    scratch = 1
    hours = 24
    s = "qsub -l select={0}:ncpus={1}:mem={2}gb:scratch_local={3}gb -l walltime={4}:00:00 ".format(select, ncpus, mem,
                                                                                                   scratch,
                                                                                                   hours) + skript_folder + "/" + skript_name
    return s

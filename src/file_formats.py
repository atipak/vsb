import json
import os


def to_json(train_data, valid_data, test_data, metadata, output_path, sdf_path):
    """
    Convert input data into a file prepared for transport through the network to a target node.
    :param train_data: Dict with two element containing train data. First part are active molecules (key: active). Second part are decoys (key: inactive).
    :param test_data: Dictionary with test data.
    :param metadata: Dictionary with metadata.
    :param valid_data: Dict with two element containing validation data. First part are active molecules (key: active). Second part are decoys (key: inactive).
    :param output_path: Path where the finish file should be located.
    :param sdf_path: Path with file containing SDF description of molecules.
    :return: Output path
    """
    if not output_path is None and not output_path == '':
        path_dir = os.path.dirname(output_path)
        if not os.path.exists(path_dir) and not path_dir == '':
            os.makedirs(os.path.dirname(output_path))
        files = []
        files.append({"path": sdf_path})
        with open(output_path, 'w') as output:
            json.dump({
                'metadata': metadata,
                'data': {
                    'files': files,
                    'train': {
                        'actives': train_data['active'],
                        'decoys': train_data['inactive']
                    },
                    'validation': {
                        'actives': valid_data['active'],
                        'decoys': train_data['inactive']
                    },
                    'test': test_data
                },
            }, output, indent=2)
        return output_path


def export_dataset(active, inactive, metadata, path):
    """
    Exports the data set into file located by given path.
    :param active: Active molecules in data set.
    :param inactive: Inactive molecules in data set.
    :param metadata: Metadata joined to data set.
    :param path: Path to the output file.
    :return: Path
    """
    if not path is None and not path == '':
        path_dir = os.path.dirname(path)
        if not os.path.exists(path_dir) and not path_dir == '':
            os.makedirs(os.path.dirname(path))
        with open(path, 'w') as output:
            json.dump({
                'metadata': metadata,
                'data': {
                    'active': active,
                    'inactive': inactive,
                },
            }, output, indent=2)
        return path


def export_selection(train, valid, test, metadata, path):
    """
    Exports given data into file which is located on place where argument path points.
    :param train_active: Names of molecules which are active in data set and are marked like train.
    :param train_inactive: Names of molecules which are inactive in data set and are marked like train.
    :param test: Names of the rest of molecules from data set determined like test molecules.
    :param metadata: Metadata of given selection. Type: dictionary
    :param path: Output file path.
    :return: Path
    """
    if not path is None and not path == '':
        path_dir = os.path.dirname(path)
        if not os.path.exists(path_dir) and not path_dir == '':
            os.makedirs(os.path.dirname(path))
        with open(path, 'w') as output:
            json.dump({
                'metadata': metadata,
                'data': {
                    'train': {
                        'active': train['active'],
                        'inactive': train['inactive']
                    },
                    'validation': {
                        'active': valid['active'],
                        'inactive': valid['inactive']
                    },
                    'test': test
                },
            }, output, indent=2)
        return path


def import_dataset(path_data, path_sdf):
    """
    Imports data set from given file.
    :param path_data: File with input data.
    :param path_sdf: File with SDF description of molecules used in data set.
    :return: List with elements: active molecules, inactive molecules, metadata, string containing SDF description.
    """
    if not os.path.exists(path_data):
        return None
    else:
        with open(path_data, 'r') as inp:
            reader = json.load(inp)
            active = []
            for a in reader['data']['active']:
                active.append(a)
            inactive = []
            for i in reader['data']['inactive']:
                inactive.append(i)
            metadata = []
            if "metadata" in reader:
                metadata = reader['metadata']
            with open(path_sdf, 'r') as sdf_file:
                sdf = sdf_file.read()
            res = []
            res.append(active)
            res.append(inactive)
            res.append(metadata)
            res.append(sdf)
            return res


def import_selection(path_data):
    """
    Imports selection from given file.
    :param path_data: File with input data.
    :return: List with these elements: metadata, train[active][inactive], test molecules.
    """
    if not os.path.exists(path_data):
        return None
    else:
        with open(path_data, 'r') as inp:
            reader = json.load(inp)
            active = []
            for a in reader['data']['train']['active']:
                active.append(a)
            inactive = []
            for i in reader['data']['train']['inactive']:
                inactive.append(i)
            test = []
            for t in reader['data']['test']:
                test.append(t)
            metadata = []
            for m in reader['metadata']:
                metadata.append(m)
            metadata = test = reader['metadata']
            train_active = reader['data']['train']['active']
            train_inactive = reader['data']['train']['inactive']
            test = reader['data']['test']
            validation_active = []
            validation_inactive = []
            if "validation" in reader['data']:
                if "actives" in reader['data']['validation']:
                    validation_active = reader['data']['validation']['active']
                if "inactives" in reader['data']['validation']:
                    validation_inactive = reader['data']['validation']['inactive']
            return {"train_active": train_active, "train_inactive": train_inactive, "test": test,
                    "validation_active": validation_active, "metadata": metadata,
                    "validation_inactive": validation_inactive}


def create_tests_input(tests, metadatas):
    """
    Creates json file with input data (thought tests) for application.
    :param tests: List with data of tests.
    :param metadatas: Metadata to tests.
    :return: Path to input file.
    """
    i = 0
    t = {}
    for test in tests:
        t[str(i)] = test
    if 'ignore' not in metadatas:
        metadatas['ignore'] = []
    if 'parallel_pc' not in metadatas:
        metadatas['parallel_pc'] = 'no'
    metadata = {"test_count": len(tests), "ignore": metadatas['ignore'], "parallel_pc": metadatas["parallel_pc"]}
    if not os.path.exists(get_temp_folder_path()):
        os.mkdir(get_temp_folder_path())
    import time
    temp_dir = get_temp_folder_path() + os.sep + 't_' + str(time.time())
    os.mkdir(temp_dir)
    temp_path = temp_dir + os.sep + 'input.json'
    with open(temp_path, 'w') as file:
        json.dump({
            'test': t,
            'metadata': metadata
        }, file, indent=2)
    return temp_path


def get_temp_folder_path():
    """
    Returns a path of the temp folder.

    :return: The path
    """
    return "../temp"


def write_query_result(results, column_names, output_file_path, success, **args):
    """
    Write a result of a query into a file.

    :param results: Results of query.
    :param column_names: Names of columns.
    :param output_file_path: Path to a file where the results should be written.
    :param success: True/False by success of the query.
    :return: Nothing
    """
    import datetime
    d = {}
    data = {}
    if not isinstance(results, (list, tuple)):
        d["success"] = False
        d["column_names"] = []
        d["data"] = {}
        d["Error detail"] = str(results)
    else:
        i = 0
        for r in results:
            name = "data_{0}".format(i)
            r = list(r)
            l = 0
            for item in r:
                if isinstance(item, datetime.date):
                    r[l] = item.isoformat()
                l += 1
            data[name] = r
            i += 1
        for a in args:
            d[a] = args[a]
        d["success"] = success
        d["column_names"] = list(column_names)
        d["data"] = data
    with open(output_file_path, "w") as stream:
        json.dump(
            d, stream, indent=2
        )


def write_run_query_result(context, output_file_path, success, **args):
    """
    Writes a result of a run query into a file.
    :param context: Result dictionary
    :param output_file_path: Path to a file where the results should be written.
    :param success: True/False by success of the query.
    :param args: Next things
    :return: Nothing
    """
    d = {}
    d["success"] = success
    d["data"] = context
    with open(output_file_path, "w") as stream:
        json.dump(
            d, stream, indent=2
        )

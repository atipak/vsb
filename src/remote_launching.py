import pysftp, time, file_formats
from general import *

configuration_file_name = 'configuration.json'
test_prefix = "test_"
refreshing_speed = 10
max_refresh_time = 300
runtimes = {}


def multi_pc_service(config, dbs_config):
    """
    Main function for executing tests on remote pcs.
    :param config: Main configuration
    :return: Nothing, SQL ID of test
    """
    # removes security option Host keys
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    # basic data, information
    config, select, recid_test = start_and_check_test(config, sql_database.DatabaseHelper.Test_types.remove)
    # check if pc_to_use file exists
    if not os.path.exists(config['pc_to_use']):
        print(config['pc_to_use'] + "not found")
        return False, recid_test
    # loading of remote pcs
    with open(config['pc_to_use'], "r") as file:
        f = json.load(file)
        pcs_maybe = []
        for pc in f['pcs']:
            pcs_maybe.append(pc)
    # checking availability
    pcs = []
    for pc in pcs_maybe:
        try:
            with pysftp.Connection(pc['hostname'], username=pc['username'], password=pc['password'], cnopts=cnopts):
                pcs.append(pc)
        except Exception as e:
            print("PC with parameters cannot be connected: ", e)
    # no remote pc for executing test therefore launch local_launching
    if len(pcs) == 0:
        import local_launching
        return local_launching.local_pc_service(config, dbs_config)
    # cycle for executing tests
    repeat = int(config['repeat'])
    # end condition - bad test - max selection which can be used
    max_number = get_max_repeat(config)
    successful = 0
    selected = {"new_selected": select}
    while actual_done(config, len(select), successful) <= repeat:
        successful += remote_pc(config, selected['new_selected'], pcs, config['test_id'], config['test_path'],
                                config['first_result'] + successful, recid_test)
        selected = choose_selections_after(repeat - successful, select)
        select = selected['all']
        if successful == repeat:
            break
        if len(select) > max_number:
            print("Too many selection used")
            return False, recid_test
    return True, recid_test


def run_test(sftp, config, pc, dataset, selection, d_remote, commands):
    """
    Runs a test on remote computer pc over sftp on given data-set and selection. Paths from d_remote are used.

    :param sftp: Binding to a remote computer.
    :param config: Test configuration
    :param pc: Information about remote pc
    :param dataset: Data-set
    :param selection: Selection
    :param d_remote: Remote paths
    :param commands: Commands for given remote computer.
    :return:
    """
    # create configuration and files for sending on remote computer. Paths are related to local pc
    d_local = create_files_for_remote(config, pc['hostname'], dataset, selection)
    # writing file with paths for remote computer
    make_file_of_configuration(d_remote, d_local['config_file_path'])
    transfer_files(sftp, d_local, d_remote)
    print("A test is launched on data-set:", dataset, " and selection:", selection)
    # launches test on remote computer
    with sftp.cd(d_remote['test_folder']):
        print(sftp.execute(command=commands['command_run']))


def get_files_from_remote(sftp, d_remote, test_path):
    """
    Gets remote files from remote computer.

    :param sftp: Binding to remote computer
    :param d_remote: Dictionary with remote paths
    :param test_path: Test path on local computer
    :return:
    """
    if sftp.exists(d_remote['stdout_tr_path']):
        print("Transferring {0}".format(os.path.basename(d_remote['stdout_tr_path'])))
        path = os.path.join(test_path, os.path.basename(d_remote['stdout_tr_path']))
        sftp.get(localpath=path, remotepath=d_remote['stdout_tr_path'])
    if sftp.exists(d_remote['stderr_tr_path']):
        print("Transferring {0}".format(os.path.basename(d_remote['stderr_tr_path'])))
        path = os.path.join(test_path, os.path.basename(d_remote['stderr_tr_path']))
        sftp.get(localpath=path, remotepath=d_remote['stderr_tr_path'])
    if sftp.exists(d_remote['vsb_stdout_tr_path']):
        print("Transferring {0}".format(os.path.basename(d_remote['vsb_stdout_tr_path'])))
        path = os.path.join(test_path, os.path.basename(d_remote['vsb_stdout_tr_path']))
        sftp.get(localpath=path, remotepath=d_remote['vsb_stdout_tr_path'])
    if sftp.exists(d_remote['vsb_stderr_tr_path']):
        print("Transferring {0}".format(os.path.basename(d_remote['vsb_stderr_tr_path'])))
        path = os.path.join(test_path, os.path.basename(d_remote['vsb_stderr_tr_path']))
        sftp.get(localpath=path, remotepath=d_remote['vsb_stderr_tr_path'])


def remote_pc(config, selections, pcs, id_test, test_path, first_result, recid_test):
    """
    A basic function for starting a test on a remote computer.

    :param config: Configuration.
    :param selections: Selected selections
    :param pcs: Computers which are ready for launching a test.
    :param id_test: ID of test.
    :param test_path: A path to a folder which is associated with test.
    :param first_result: ???
    :param recid_test: ???
    :return: Count of successful tests
    """
    # removes security option Host keys
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    # count of successful tests
    successful = 0
    # helping variable
    i = 0
    # solving selections: key: name of pc, value: dataset_selection
    solved_selections = {}
    check_temp_folder()
    while i < len(selections):
        for pc in pcs:
            # solving tests is lower than is required
            if i < len(selections):
                dataset = selections[i]['dataset']
                selection = selections[i]['selection']
                launched_test = True
                try:
                    # opening connection to remote pc
                    with pysftp.Connection(pc['hostname'], username=pc['username'], password=pc['password'],
                                           cnopts=cnopts) as sftp:
                        print("Connected to PC:", pc['hostname'])
                        # adding new test into helping dictionary with solving tests, no test is running on remote pc
                        if not pc['hostname'] in solved_selections:
                            solved_selections[pc['hostname']] = dataset + "#" + selection
                            launched_test = False
                        # test is running on remote pc, getting data-set and selection
                        dataset, selection = get_dataset_selection_from_key(solved_selections[pc['hostname']])
                        # getting method id
                        method_id = get_method_id(config)
                        # getting OS
                        OS = get_os_class(pc['OS_type'])
                        # getting specific separator for paths on remote pc
                        remote_path_separator = OS.path_separator
                        # preparing other files paths
                        d_remote = config_for_remote(config, method_id, dataset, selection, remote_path_separator)

                        commands = write_commands(d_remote["test_folder"], d_remote['configuration'], OS)
                        # creating paths
                        if not launched_test:
                            if not sftp.exists(d_remote["method_folder"]):
                                sftp.makedirs(d_remote['method_folder'])
                        #########################################
                        # run new test
                        if not launched_test:
                            if not sftp.exists(d_remote["test_folder"]):
                                sftp.mkdir(d_remote["test_folder"])
                            runtimes[pc['hostname']] = time.time()
                            run_test(sftp, config, pc, dataset, selection, d_remote, commands)
                            i += 1
                            launched_test = True
                            continue  # to another pc
                        # check results
                        else:
                            # tests if test is finished
                            with sftp.cd(d_remote["test_folder"]):
                                continuing = sftp.execute(command=commands['command_proof'])
                            # something bad happened restart test
                            if len(continuing) == 0:
                                print("Test checking wasn't succeeded")
                                get_files_from_remote(sftp, d_remote, test_path)
                                clean_remote(sftp, OS, d_remote["test_folder"])
                                clean_local_temp(pc['hostname'])
                                solved_selections.pop(pc['hostname'], None)
                                continue  # to another computer
                            else:
                                state = str(continuing[0])
                                # test is not over
                                if not state.find("yes") == -1:
                                    continue  # to another computer
                                # test is over
                                elif not state.find("no") == -1:
                                    print("Checking test data-set_selection {0}".format(d_remote['dataset'] + "_" + d_remote['selection']))
                                    if write_result(sftp, d_remote, commands['command_success'], id_test,
                                                    successful + 1,
                                                    test_path, pc['hostname'], config, recid_test,
                                                    int(time.time() - runtimes[pc['hostname']])):
                                        successful += 1
                                    get_files_from_remote(sftp, d_remote, test_path)
                                    clean_remote(sftp, OS, d_remote["test_folder"])
                                    clean_local_temp(pc['hostname'])
                                    solved_selections.pop(pc['hostname'], None)
                                    reset_sleep_time()
                                    continue  # another computer
                                else:
                                    raise RemoteException(
                                        "Vysledek zkousky probihajiciho testu nerozpoznan: " + str(continuing))
                except RemoteException as re:
                    print(re)
                    i += 1
                except Exception as e:
                    print(e)
                    i += 1
            else:
                if not pc['hostname'] in solved_selections:
                    continue
                with pysftp.Connection(pc['hostname'], username=pc['username'], password=pc['password'],
                                       cnopts=cnopts) as sftp:
                    print("Connected to PC:", pc['hostname'])
                    dataset, selection = get_dataset_selection_from_key(solved_selections[pc['hostname']])
                    method_id = get_method_id(config)
                    OS = get_os_class(pc['OS_type'])
                    remote_path_separator = OS.path_separator
                    d_remote = config_for_remote(config, method_id, dataset, selection, remote_path_separator)
                    if sftp.exists(d_remote["test_folder"]):
                        print("Task on computer {0} is still running.".format(pc['hostname']))
                        break
        print("Main thread is going to sleep")
        time.sleep(get_sleep_time())
    # there are still running tests - waiting
    waiting = True
    while waiting:
        waiting = False
        for pc in pcs:
            if pc['hostname'] not in solved_selections:
                continue
            with pysftp.Connection(pc['hostname'], username=pc['username'], password=pc['password'],
                                   cnopts=cnopts) as sftp:
                print("Connected to PC:", pc['hostname'])
                dataset, selection = get_dataset_selection_from_key(solved_selections[pc['hostname']])
                method_id = get_method_id(config)
                OS = get_os_class(pc['OS_type'])
                remote_path_separator = OS.path_separator
                d_remote = config_for_remote(config, method_id, dataset, selection, remote_path_separator)
                commands = write_commands(d_remote["test_folder"], d_remote['configuration'], OS)
                if sftp.exists(d_remote["test_folder"]):
                    with sftp.cd(d_remote["test_folder"]):
                        continuing = sftp.execute(command=commands['command_proof'])
                    if len(continuing) == 0:
                        print("Test checking wasn't succeeded")
                        get_files_from_remote(sftp, d_remote, test_path)
                        clean_remote(sftp, OS, d_remote["test_folder"])
                        clean_local_temp(pc['hostname'])
                        solved_selections.pop(pc['hostname'], None)
                        continue  # to another computer
                    else:
                        state = str(continuing[0])
                        if not state.find("yes") == -1:
                            waiting = True
                            continue
                        elif not state.find("no") == -1:
                            print("Checking test data-set_selection {0}".format(
                                d_remote['dataset'] + "_" + d_remote['selection']))
                            if write_result(sftp, d_remote, commands['command_success'], id_test, successful + 1,
                                            test_path,
                                            pc['hostname'], config, recid_test, int(time.time() - runtimes[pc['hostname']])):
                                successful += 1
                            get_files_from_remote(sftp, d_remote, test_path)
                            clean_remote(sftp, OS, d_remote["test_folder"])
                            clean_local_temp(pc['hostname'])
                            solved_selections.pop(pc['hostname'], None)
                            reset_sleep_time()
        print("Main thread is going to sleep")
        time.sleep(get_sleep_time())
    return successful


def write_result(sftp, d_remote, check_command, id_test, id_result, test_path, pc_name, data, recid_test, runtime=0):
    """
    Check if test succeeded on the remote computer. Then it launches procedure inserting.

    :param recid_test: SQL ID of test
    :param sftp: SFTP connection
    :param d_remote: A dictionary with paths on the remote computer.
    :param check_command: Command which checks on the remote computer if test was succeeded.
    :param id_test: ID of test.
    :param id_result: ID of result
    :param test_path: A path to a folder which is associated with test.
    :param pc_name: A name of a remote PC.
    :param data: Some data.
    :return: True/False if inserting succeeded.
    """
    print("Checking of a remote computer's result")
    with sftp.cd(d_remote["test_folder"]):
        l_result = sftp.execute(check_command)
    state = str(l_result[0])
    if not state.find('yes') == -1:
        return success(sftp, d_remote, id_test, id_result, test_path, pc_name, data, recid_test, runtime)
    else:
        print("The test failed")
        return False


def success(sftp, d_remote, id_test, id_result, test_path, pc_name, data, recid_test, runtime=0):
    """
    Checks if a result file exists and if the condition is true then downloads the result file and inserts result into DB.

    :param recid_test: SQL ID of test
    :param sftp: SFTP connection.
    :param d_remote: Dictionary with remote paths.
    :param id_test: ID of test.
    :param id_result: ID of result.
    :param test_path: A path to a folder which is associated with test.
    :param pc_name: A name of a remote PC.
    :param data: Some data.
    :param runtime: runtime
    :return: True/False if inserting succeeded.
    """
    print("The test succeeded")
    check_temp_folder()
    temp_path = get_temp_folder_path() + os.sep + pc_name
    temp_result = temp_path + os.sep + 'result.json'
    if not sftp.exists(d_remote['result_tr_path']):
        print("The result not found")
        return False
    else:
        print("Transfering of the test result from a remote computer:")
        sftp.get(localpath=temp_result, remotepath=d_remote['result_tr_path'])
        return insert_data_into_sql_database(temp_result, id_test, d_remote['file_type'], d_remote['dataset'],
                                             d_remote['selection'], test_path, id_result, recid_test, runtime)


def write_commands(remote_path_pc, configuration_path, OS):
    """
    Writes commands into a dictionary for a remote computer.

    :param remote_path_pc: A path where the launch file is stored.
    :param remote_path_separator: A path separator which is used on a remote computer.
    :param configuration_path: A path where a configuration file is stored on the remote computer.
    :param OS: Type of operation system on the remote computer.
    :return: Dictionary with commands
    """
    directory = {}
    if remote_path_pc == "":
        remote_path_separator = ""
    # zkouska dokonceni
    directory[
        "command_proof"] = "cd {0}".format(
        remote_path_pc) + OS.command_separator + 'python ' + 'remote_station.py -z ' + configuration_path
    # spusteni skriptu
    directory[
        "command_run"] = OS.ret_suspend_commander("cd {0}".format(
        remote_path_pc) + OS.command_separator + 'python ' + 'remote_station.py -s ' + configuration_path)
    # ukonceni, uklizeni po provadeni skriptu
    directory[
        "command_success"] = "cd {0}".format(
        remote_path_pc) + OS.command_separator + 'python ' + 'remote_station.py -u ' + configuration_path
    directory[
        'command_metacentrum'
    ] = 'python ' + 'remote_station.py -m ' + configuration_path
    return directory


def clean_remote(sftp, OS, remote_path):
    """
    Clean a remote computer. Remove recursively all files at given path.

    :param sftp: SFTP connection.
    :param OS: Operation system of a remote computer
    :param remote_path: A remote path which should be removed.
    :return: Nothing
    """
    print("Remote computer cleaning")
    sftp.execute(OS.ret_remove_rec_com(folder_name=remote_path))


def clean_local_temp(pc_name):
    """
    Clean the local computer. Remove all files saved in folder for temporally files.
    :param pc_name: PC name like identification of folder of temporally files for given task.
    :return: Nothing
    """
    print("Local temp cleaning")
    pwd = get_temp_folder_path() + os.sep + pc_name
    import shutil
    shutil.rmtree(pwd)


def transfer_files(sftp, d_local, d_remote):
    """
    Transfers files to a remote computer.
    :param sftp: SFTP connection
    :param d_local: Dictionary with local paths
    :param d_remote: Dictionary with remote paths.
    :return:
    """
    print("Creating a zip file for a transferring of files")
    import zipfile
    zip_file = zipfile.ZipFile(d_local['zip_file'], mode="w")
    if os.path.exists(d_local['dataset_path']):
        zip_file.write(d_local['dataset_path'], arcname=os.path.basename(d_local['dataset_path']))
    if os.path.exists(d_local['input_data_path']):
        zip_file.write(d_local['input_data_path'], arcname=os.path.basename(d_local['input_data_path']))
    if os.path.exists(d_local['sdf_path']):
        zip_file.write(d_local['sdf_path'], arcname=os.path.basename(d_local['sdf_path']))
    if os.path.exists(d_local['skeleton_path']):
        zip_file.write(d_local['skeleton_path'], arcname=os.path.basename(d_local['skeleton_path']))
    if 'repr_method_path' in d_remote:
        add_folder_to_zip_file(zip_file, d_local["repr_method_path"])
    if 'sim_method_path' in d_remote:
        add_folder_to_zip_file(zip_file, d_local["sim_method_path"])
    if "file_method_path" in d_remote:
        add_folder_to_zip_file(zip_file, d_local["file_method_path"])
    zip_file.close()
    print("The zip file was created")
    print("Transferring of files configuration.json and remote_station.py")
    if os.path.exists(d_local['config_file_path']):
        sftp.put(d_local['config_file_path'], d_remote['configuration_tr_path'])
    if os.path.exists(d_local['remote_station_path']):
        sftp.put(d_local['remote_station_path'], d_remote['remote_station_tr_path'])
    print("Transferring of the zip file")
    sftp.put(d_local['zip_file'], d_remote['zip_file_tr_path'])
    print("Transferring finished")


def add_folder_to_zip_file(zip_file, path, metacentrum=False):
    if metacentrum:
        dp = path
    else:
        dp, fp = os.path.split(path)
    for root, dirs, files in os.walk(path):
        pr = str(os.path.commonpath([root, dp]))
        add_path = str(root)[len(pr):].strip("/").strip("\\")
        for file in files:
            zip_file.write(os.path.join(root, file), arcname=os.path.join(add_path, file))


def create_files_for_remote(config, pc_name, dataset, selection):
    """
    Creates paths and files for transporting to a remote computer.

    :param config: Configuration.
    :param config_file_path: A path where a configuration file should be stored.
    :param pc_name: A name of PC
    :param dataset: ID of dataset.
    :param selection: ID of selection.
    :return: Dictionary with paths.
    """
    print("Creating of base files for a launching of a test on a remote computer")
    dictionary = {}
    check_temp_folder()
    local_pc_temp_path = os.path.join(get_temp_folder_path(), pc_name)
    config_file_path = os.path.join(local_pc_temp_path, configuration_file_name)
    try:
        os.mkdir(local_pc_temp_path)
    except:
        print(local_pc_temp_path, "already exists")
    dictionary['dataset_path'] = os.path.join(local_pc_temp_path, 'selection.json')
    dictionary['input_data_path'] = os.path.join(local_pc_temp_path, 'input.json')
    dictionary['sdf_path'] = file_database.get_path_sdf(dataset)
    dictionary['config_file_path'] = config_file_path
    dictionary['skeleton_path'] = 'skeleton.py'
    dictionary['remote_station_path'] = 'remote_station.py'
    dictionary['zip_file'] = os.path.join(local_pc_temp_path, 'data_for_transport.zip')
    train = file_database.get_selection_train(dataset, selection)
    valid = file_database.get_selection_valid(dataset, selection)
    test = file_database.get_selection_test(dataset, selection)
    metadata = []
    if config['typefile'] == 'python':
        dictionary['sim_method_path'] = config['sim_method_path']
        dictionary['repr_method_path'] = config['repr_method_path']
        write_selection_file_for_remote(metadata, train, valid, test, dictionary['dataset_path'])
    else:
        dictionary['file_method_path'] = config['file_method_path']
        file_formats.to_json(train, valid, test, metadata, dictionary['input_data_path'], dictionary['sdf_path'])
    return dictionary


def config_for_remote(config, method_id, dataset, selection, remote_path_separator, metacentrum=False):
    """
    Create a directory with paths of files stored on remote PC.
    :param config: Test configuration
    :param method_id: ID of method
    :param dataset: ID of data set
    :param selection: ID of selection
    :param remote_path_separator: Path separator on remote PC
    :return: A dictionary
    """
    directory = {}
    # base paths and information
    directory['base_folder_path'] = "VSB_folder"
    directory["method_id"] = method_id
    directory['dataset'] = str(dataset)
    directory['selection'] = str(selection)
    directory['file_type'] = config['typefile']
    if "optimalization" in config:
        directory['optimalization'] = config['optimalization']
    directory['method_folder'] = directory['base_folder_path'] + remote_path_separator + directory['method_id']
    test_folder_name = test_prefix + str(dataset) + "_" + str(selection)
    directory["test_folder"] = directory['method_folder'] + remote_path_separator + test_folder_name
    # metacentrum paths
    directory['relative_method_folder'] = directory['method_id']
    directory['relative_test_folder'] = directory['relative_method_folder'] + remote_path_separator + test_folder_name
    directory['relative_configuration'] = directory[
                                              'relative_test_folder'] + remote_path_separator + configuration_file_name

    # paths for transferring
    directory["success_tr_path"] = directory["test_folder"] + remote_path_separator + 'success.txt'
    directory["state_tr_path"] = directory["test_folder"] + remote_path_separator + 'state.txt'
    directory["sdf_tr_path"] = directory["test_folder"] + remote_path_separator + 'mols.sdf'
    directory["stdout_tr_path"] = directory["test_folder"] + remote_path_separator + 'stdout.txt'
    directory["stderr_tr_path"] = directory["test_folder"] + remote_path_separator + 'stderr.txt'
    directory["vsb_stdout_tr_path"] = directory["test_folder"] + remote_path_separator + 'vsb_stdout.txt'
    directory["vsb_stderr_tr_path"] = directory["test_folder"] + remote_path_separator + 'vsb_stderr.txt'
    directory["selection_tr_path"] = directory["test_folder"] + remote_path_separator + 'selection.json'
    directory["result_tr_path"] = directory["test_folder"] + remote_path_separator + 'result.json'
    directory["output_tr_path"] = directory["test_folder"] + remote_path_separator + 'result.json'
    directory["configuration_tr_path"] = directory["test_folder"] + remote_path_separator + 'configuration.json'
    directory["remote_station_tr_path"] = directory["test_folder"] + remote_path_separator + 'remote_station.py'
    directory["skeleton_tr_path"] = directory["test_folder"] + remote_path_separator + 'skeleton.py'
    directory['input_tr_path'] = directory["test_folder"] + remote_path_separator + 'input.json'
    directory['zip_file_tr_path'] = directory["test_folder"] + remote_path_separator + 'tr_data.zip'
    # remote paths
    directory["success"] = 'success.txt'
    directory["state"] = 'state.txt'
    if metacentrum:
        if directory['file_type'] == 'general':
            directory["sdf"] = ".." + remote_path_separator + "sdf_folder" \
                               + remote_path_separator + str(dataset) + remote_path_separator + "mols.sdf"
        else:
            directory["sdf"] = ".." + remote_path_separator + "sdf_folder" \
                           + remote_path_separator + str(dataset) + remote_path_separator + "mols.sdf"
    else:
        directory["sdf"] = 'mols.sdf'
    directory["vsb_stdout"] = 'vsb_stdout.txt'
    directory["vsb_stderr"] = 'vsb_stderr.txt'
    directory["stdout"] = 'stdout.txt'
    directory["stderr"] = 'stderr.txt'
    directory["f_selection"] = 'selection.json'
    directory["result"] = 'result.json'
    directory["output"] = 'result.json'
    directory["configuration"] = configuration_file_name
    directory["remote_station"] = 'remote_station.py'
    directory["skeleton"] = 'skeleton.py'
    directory['input'] = 'input.json'
    directory['zip_file'] = 'tr_data.zip'
    # type dependent data
    if directory['file_type'] == 'general':
        directory['file_method_path'] = os.path.basename(config['file_method_path'])
        directory['execute_command'] = config['execute_command']
    else:
        directory['repr_method_path'] = os.path.basename(str(config['repr_method_path']))
        directory['sim_method_path'] = os.path.basename(str(config['sim_method_path']))
    return directory


def make_file_of_configuration(d_remote, configuration_path):
    """
    Creates file filled with configuration data.

    :param d_remote: Dictionary from the function config_for_remote.
    :param configuration_path: Path where the configuration file should be stored
    :return: configuration_path
    """
    print("Creating a file configuration.json")
    config_data = {}
    for key in d_remote:
        if not str(key).endswith("tr_path"):
            config_data[str(key)] = d_remote[str(key)]
    with open(configuration_path, "w") as file:
        json.dump(config_data, file, indent=2)
    return configuration_path


def write_selection_file_for_remote(metadata, train, valid, test, selection_file_path):
    """
    Writes a selection file for launching test on remote PC.
    :param metadata: Metadata of selection.
    :param train: Train molecules of selection.
    :param test: Test molecules of selection.
    :param selection_file_path: Path where the selection file should be stored
    :return: Nothing
    """
    print("Creating a file selection.json")
    with open(selection_file_path, "w") as file:
        json.dump({
            'data': {
                'train': train,
                'test': test,
                "validation": valid
            },
            "metadata": metadata
        }, file)


def get_dataset_selection_from_key(key):
    """
    Returns parsed key.
    :param key: key
    :return: Data set ID,
    """
    parts = str(key).strip().split("#")
    return parts[0], parts[1]


def get_method_id(config):
    """
    Returns a ID for remote computer which marks the test.

    :param config: Configuration.
    :return: ID
    """
    if config["typefile"] == "python":
        s_m = os.path.basename(config["sim_method_path"]).replace(".py", "")
        r_m = os.path.basename(config["repr_method_path"]).replace(".py", "")
        return s_m + r_m
    else:
        return os.path.basename(config["file_method_path"]).replace(".py", "")


def get_sleep_time():
    """
    Returns sleep time.
    :return: Integer in second
    """
    global refreshing_speed
    act_speed = refreshing_speed
    refreshing_speed += int(refreshing_speed / 2)
    if refreshing_speed > max_refresh_time:
        refreshing_speed = max_refresh_time
    return act_speed


def reset_sleep_time():
    """
    Resets sleep time
    :return:
    """
    global refreshing_speed
    refreshing_speed = 10

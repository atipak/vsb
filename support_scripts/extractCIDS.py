import json, os


def main():
    """
    Function which extracts cids to a file.

    :return: Nothing
    """
    print("Hello")
    print("Please insert a path to the 'newDataset.json' file created by script 'fromCSVtoJSON.py'")
    path = str(input("Path: ")).strip("\"")
    while True:
        if not os.path.exists(path):
            print("Your path doesn't exist. Please insert the path again")
        else:
            break
    print("Creating cids file in actual folder")
    molecules = []
    with open(path) as stream:
        js = json.load(stream)
        for act in js["data"]["active"]:
            molecules.append(act)
        for inact in js["data"]["inactive"]:
            molecules.append(inact)
    with open("cids.txt", "w") as file:
        file.write(",".join(molecules))

if __name__ == '__main__':
    main()

import math


def scoring(repr, train, valid, test):
    """
    A method for computing of scores: Euclide metric

    :param repr: A dictionary with representations of molecules from loading function
    :param train: A dictionary with lists of active, inactive train molecules names
    :param valid: A dictionary with lists of active, inactive validation molecules names
    :param test: A list with test molecules names
    :return: A list with dictionaries, where keys are "name", "similarity"[, "score_molecule", "description"]
    """
    active = train["active"]
    result = []
    for t in test:
        max_sim = -1
        max_name = ""
        for act in active:
            dist = get_distance(repr[t], repr[act])
            if max_sim < dist:
                max_sim = dist
                max_name = act
        result.append({
            'name': t,
            'similarity': max_sim,
            'score_molecule': max_name
        })
    return result


def get_distance(f_one, f_two):
    """
    Returns Euclidean distance of two fingerprints
    :param f_one: first molecule's fingerprint
    :param f_two: second molecule's fingerprint
    :return: distance
    """
    if not len(f_one) == len(f_two):
        raise Exception("Lengths of fingerprints are not same.")
    count = 0
    for i in range(0, len(f_one)):
        count += pow((f_one[i] - f_two[i]), 2)
    return math.sqrt(count)

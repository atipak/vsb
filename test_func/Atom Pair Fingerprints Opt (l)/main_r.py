from rdkit.Chem.Fingerprints import FingerprintMols


def loading(molecule):
    """
    A method for representation of molecules: AtomPairFingerprint

    :param molecule: A RDkit molecule object
    :return: Representation of molecule.
    """
    if molecule is None:
        return None
    fp = FingerprintMols.FingerprintMol(molecule)
    return fp



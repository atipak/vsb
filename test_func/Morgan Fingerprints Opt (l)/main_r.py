from rdkit.Chem import AllChem



def loading(molecule):
    """
    A method for representation of molecules: Morgan fingerprints

    :param molecule: A RDkit molecule object
    :return: Representation of molecule.
    """
    if molecule is None:
        return None
    fp = AllChem.GetMorganFingerprint(molecule, 2)
    return fp




